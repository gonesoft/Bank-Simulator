﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Producto
    {
        public string Nombre { get; set; }

        public static List<Producto> obtenerTodos()
        {
            List<Producto> tmp = new List<Producto>();
            tmp.Add(new Producto() { Nombre = "Cuenta de Ahorro" });
            tmp.Add(new Producto() { Nombre = "Cuenta Débito Popular" });
            tmp.Add(new Producto() { Nombre = "Cuenta Corriente Tradicional" });
            tmp.Add(new Producto() { Nombre = "Cuenta Corriente Transaccional" });
            tmp.Add(new Producto() { Nombre = "VISA Débito local" });
            tmp.Add(new Producto() { Nombre = "Visa Débito Clásica" });
            tmp.Add(new Producto() { Nombre = "VISA Débito Gold" });
            tmp.Add(new Producto() { Nombre = "VISA Clásica" });
            tmp.Add(new Producto() { Nombre = "MasterCard Clásica" });
            tmp.Add(new Producto() { Nombre = "VISA Gold local" });
            tmp.Add(new Producto() { Nombre = "VISA Gold doble saldo" });
            tmp.Add(new Producto() { Nombre = "MasterCard Gold local" });
            tmp.Add(new Producto() { Nombre = "MasterCard Gold doble saldo" });
            tmp.Add(new Producto() { Nombre = "MasterCard Orbit" });
            tmp.Add(new Producto() { Nombre = "Avanza CCN" });
            tmp.Add(new Producto() { Nombre = "MasterCard Almacenes Iberia" });
            tmp.Add(new Producto() { Nombre = "VISA IKEA Family" });
            tmp.Add(new Producto() { Nombre = "MasterCard Caminantes por la Vida" });
            tmp.Add(new Producto() { Nombre = "VISA Pola Sirena" });
            tmp.Add(new Producto() { Nombre = "VISA Utesa" });
            tmp.Add(new Producto() { Nombre = "MasterCard Universal de Seguros" });
            tmp.Add(new Producto() { Nombre = "CCN PLUS" });
            tmp.Add(new Producto() { Nombre = "MasterCard MileagePlus" });
            tmp.Add(new Producto() { Nombre = "Millas Popular" });
            tmp.Add(new Producto() { Nombre = "Préstamos" });
            tmp.Add(new Producto() { Nombre = "Préstamos Relacionados Comerciales" });
            tmp.Add(new Producto() { Nombre = "Vida Popular" });
            tmp.Add(new Producto() { Nombre = "Complemento Hospitalario" });
            tmp.Add(new Producto() { Nombre = "Certificados de Depósito" });
            tmp.Add(new Producto() { Nombre = "Depósito a plazo fijo" });
            tmp.Add(new Producto() { Nombre = "Depósito a plazo fijo dólares" });

            return tmp;
        }
    }
}
