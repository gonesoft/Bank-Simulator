﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Caso
    {
        public string Titulo { get; set; }
        public string Text { get; set; }

        public static List<Caso> ObtenerTodos(int Trimestre, int BancoID, int PartidaID)
        {
            Indicador indicador = Indicador.ObtenerIndicadorPorTrimestreYBancoIDYPartidaID(Trimestre, BancoID, PartidaID);
            List<Caso> tmp = new List<Caso>();
            string capitalDeTrabajo = "";

            string capacidadCrediticia = "";
            if (indicador != null)
            {
                capitalDeTrabajo = (indicador.CapitalTrabajo + indicador.CapacidadCrediticia + indicador.ReservasCapital - indicador.CreditosPendientes).ToMoney();
                capacidadCrediticia = indicador.CapacidadCrediticia.ToMoney();
            }


            tmp.Add(new Caso() { Titulo = "CUENTAS DE AHORRO, CORRIENTE Y CERTIFICADOS FINANCIEROS", Text = "Con el propósito de fomentar una cultura de cuidado y respeto al medioambiente y fomentar la práctica del ahorro, el Banco Popular Dominicano diseñó la campaña “Ahorrar nos hace bien”. En este sentido, como una entidad bancaria socialmente responsable, deberás alcanzar en el trimestre enero-marzo, los siguientes objetivos de negocios: la colocación de 60,000 cuentas de ahorro; 120,000 cuentas corrientes; 9,000 certificados financieros, y con esto la captación del 25% de los fondos que se destinarán para el financiamiento de las nuevas propuestas de negocios de este año. Para estos fines, cuentas con una fuerza de ventas directas de <b><u>1,500 empleados</u></b>, la mejor base tecnológica del país, una tasa promedio de intereses de 1.5%, un valor en tus utilidades neta de <b><u>RD" + capitalDeTrabajo + "</u></b> al cierre del trimestre, un presupuesto de <b><u>RD" + capacidadCrediticia + "</u></b> para desarrollar este caso de negocios y cuatro casos adicionales más adelante." });
            tmp.Add(new Caso() { Titulo = "TARJETA DE CRÉDITO", Text = "La División de Mercadeo Analítico ha identificado una nueva población de 800,000 potenciales tarjetahabientes que podrían insertarse como clientes al sector financiero. Para aprovechar esta oportunidad que brinda el mercado, tienes como nuevo objetivo de negocios, en el trimestre abril-junio, colocar 200,000 unidades de tarjetas de crédito, que estarán dirigidas a un público joven-adulto, entre 20 y 40 años, estudiantes, jóvenes profesionales y trabajadores independientes con nivel socio económico B, B+, C y C+. Perfil geográfico, indistinto. Para ello, cuentas con una tasa promedio de intereses en el mercado de 5.5%, un equipo de trabajo de<strong> 1,000 empleados</strong>, un valor en tus utilidades neta de <strong><u>RD$" + capitalDeTrabajo + "</u></strong> y un presupuesto <strong><u>RD" + capacidadCrediticia + "</u></strong> para desarrollar este caso de negocios y tres casos adicionales más adelantes." });
            tmp.Add(new Caso() { Titulo = "PRÉSTAMOS HIPOTECARIOS", Text = "Desde hace unos meses el sector inmobiliario ha presentado indicadores favorables para que un alto porcentaje de las familias del país puedan adquirir una vivienda nueva o usada. En el marco de esta realidad, como objetivo de negocios debes, para el trimestre julio-septiembre, superar en un 15% el trimestre anterior, contando con una tasa promedio en el mercado de 14.5% de intereses, la disponibilidad de un equipo de trabajo de <strong>500 empleados</strong>, un valor en tus utilidades neta de <strong><u>RD" + capitalDeTrabajo  + "</u></strong>, y un presupuesto de <strong><u>RD" + capacidadCrediticia + "</u></strong> para desarrollar este caso de negocios y dos casos adicionales más adelante." });
            tmp.Add(new Caso() { Titulo = "INVERSIONES POPULAR", Text = "El mercado de valores dominicano ofrece la facilidad de obtener un alto porcentaje de retorno competitivo de las inversiones que se realicen en el trimestre octubre-diciembre del presente año. En este contexto, Inversiones Popular, S.A. – Puesto de Bolsa, se ha trazado como uno de sus objetivos de negocios la apertura de 2,000 cuentas de corretaje de nuevos clientes y superar en un 20% la colocación que en calidad de inversión se realizó en el trimestre anterior. Para esto cuenta con los siguientes instrumentos de inversión: títulos de los emisores emitidos por el Banco Central, el Ministerio de Hacienda y bonos soberanos y corporativos, los cuales son atractivos en el mercado secundario con un rango de tasa de rendimiento de 7% a 10%, un valor en tus utilidades neta de <strong><u>RD" + capitalDeTrabajo + "</u></strong> y un presupuesto de <strong><u>RD" + capacidadCrediticia + "</u></strong> para desarrollar este caso de negocios y unos casos adicionales más adelante. En adición, para minimizar los riesgos, cuentas con un equipo de trabajo de <strong>12 especialistas en inversiones</strong> distribuidos a nivel nacional y una data de todos los participantes del mercado de valores, la cual está debidamente regulada por la Superintendencia de Valores." });
            tmp.Add(new Caso() { Titulo = "AUTO FERIA POPULAR", Text = "Para alcanzar un incremento del 30% en relación al objetivo alcanzado el año anterior en la tradicional Autoferia Popular, cuentas con un equipo de apoyo directo de<strong> 5,000 empleados</strong>, más de 30 dealers autorizados por su trayectoria ética, una tasa promedio en el mercado de 12.95%, fija a cinco años, un presupuesto para este último caso de <strong><u>RD" + capitalDeTrabajo + "</u></strong>, un valor en tus utilidades neta de <strong><u>RD" + capacidadCrediticia + "</u></strong> y la misión de que 1,600 personas puedan cumplir el sueño de adquirir un vehículo nuevo." });

            return tmp;

        }

    }
}
