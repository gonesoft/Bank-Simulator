﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;


public class MailHelper
{
    public static bool sendEmail(string emailTo, string body, string subject, List<String> cc = null)
    {
        try
        {
            SmtpClient smtp = new SmtpClient("relay-hosting.secureserver.net", 25);
            string[] emails = { "info@simulator.jadom.org", "no-reply-reply@simulator.jadom.org", "no-reply-simulator@simulator.jadom.org", "no-reply@simulator.jadom.org", "noreply@simulator.jadom.org" };
            Random rnd = new Random();
            int rndIndex = rnd.Next(0, 4);

            if (rndIndex > emails.Length)
                rndIndex = 0;

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(emails[rndIndex]);
            mail.To.Add(emailTo);
            if (cc != null && cc.Count > 0)
            {
                foreach (string email in cc)
                {
                    mail.CC.Add(email);
                }
            }

            mail.Subject = subject;
            mail.IsBodyHtml = true;
            mail.Body = body;
            mail.Priority = MailPriority.Normal;
            smtp.Send(mail);
            smtp.Dispose();
            return true;
        }
        catch
        {
            return false;
        }

    }

}
