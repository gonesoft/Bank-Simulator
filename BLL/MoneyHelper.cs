﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class MoneyHelper
    {
        public static string ToMoney(this decimal d)
        {
            CultureInfo ci = new CultureInfo("en-US");

            if (d < 0)
            {
                d = d * -1;
               
                return d.ToString("C", ci);
            }

            return d.ToString("C", ci);
        }

    }
}
