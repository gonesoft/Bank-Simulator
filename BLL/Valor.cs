﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Valor
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string MiValor { get; set; }
        public List<Enunciado> EnunciadosCorrectos { get; set; }
        public List<Enunciado> EnunciadosIncorrectos { get; set; }

        public Valor()
        { }

        public static Valor ObtenerPorValorID(int id)
        {
            DataTable dt = DataAccess.GetData("ObtenerValorPorID", id);
            Valor tmp = null;

            if (dt != null && dt.Rows.Count > 0)
            {
                IEnumerable<Enunciado> enunciados = Enunciado.ObtenerEnunciadosPorValorID(id);
                tmp = (Valor)dt.Rows[0];
                tmp.EnunciadosCorrectos = new List<Enunciado>();
                tmp.EnunciadosIncorrectos = new List<Enunciado>();

                foreach (Enunciado e in enunciados)
                {
                    if (e.Tipo)
                        tmp.EnunciadosCorrectos.Add(e);
                    else
                        tmp.EnunciadosIncorrectos.Add(e);
                }
            }

            return tmp;
        }

        public static IEnumerable<Valor> ObtenerTodos()
        {
            DataTable dt = DataAccess.GetData("ObtenerValores");
            List<Enunciado> todos = Enunciado.ObtenerTodos().ToList();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Valor vl = (Valor)dr;
                    vl.EnunciadosCorrectos = new List<Enunciado>();
                    vl.EnunciadosIncorrectos = new List<Enunciado>();

                    vl.EnunciadosCorrectos = (from Enunciado e in todos where e.ValorID == vl.ID && e.Tipo select e).ToList().GetRandomElements(3);
                    vl.EnunciadosIncorrectos = (from Enunciado e in todos where e.ValorID == vl.ID && !e.Tipo select e).ToList().GetRandomElements(2);
                    yield return vl;
                }
            }
        }

        public static explicit operator Valor(DataRow dr)
        {
            Valor tmp = null;

            if (dr != null)
            {
                tmp = new Valor();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.Nombre = dr["Nombre"].ToString();
                tmp.MiValor = dr["Valor"].ToString();
            }

            return tmp;
        }
    }

    public class Enunciado
    {
        public int ID { get; set; }
        public string MiEnunciado { get; set; }
        public int ValorID { get; set; }
        public Nullable<bool> Seleccionado { get; set; }
        public int ValorSeleccionID { get; set; }
        public bool? EsCorrecta { get; set; }
        public bool Tipo { get; set; }

        public static IEnumerable<Enunciado> ObtenerEnunciadosPorValorID(int valorId)
        {
            DataTable dt = DataAccess.GetData("ObtenerEnunciadosPorValorID", valorId, "ValorID");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Enunciado)dr;
                }
            }
        }

        public static IEnumerable<Enunciado> ObtenerTodos()
        {
            DataTable dt = DataAccess.GetData("ObtenerEnunciados");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Enunciado)dr;
                }
            }
        }

        public static IEnumerable<Enunciado> ObtenerEnunciadosValoresSeleccionPorPartidaIDYUsuarioIDYValorIDYTrimestre(int partidaId, int usuarioId, int valorId, int trimestre)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", partidaId));
            paramList.Add(new SqlParameter("UsuarioID", usuarioId));
            paramList.Add(new SqlParameter("ValorID", valorId));
            paramList.Add(new SqlParameter("Trimestre", trimestre));

            DataTable dt = DataAccess.GetData("ObtenerEnunciadosValoresSeleccionPorPartidaIDYUsuarioIDYValorID", paramList);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Enunciado)dr;
                }
            }
        }

        public static IEnumerable<Enunciado> ObtenerEnunciadosSeleccionadosPorUsuarioIDYPartidaID(int partidaId, int usuarioId)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", partidaId));
            paramList.Add(new SqlParameter("UsuarioID", usuarioId));
            
            DataTable dt = DataAccess.GetData("ObtenerEnunciadosSeleccionadosPorUsuarioIDYPartidaID", paramList);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Enunciado)dr;
                }
            }
        }

        public static explicit operator Enunciado(DataRow dr)
        {
            Enunciado tmp = null;

            if (dr != null)
            {
                tmp = new Enunciado();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.ValorID = Convert.ToInt32(dr["ValorID"]);
                tmp.MiEnunciado = dr["Enunciado"].ToString();

                if (dr.Table.Columns.Contains("Seleccionado"))
                    if (dr["Seleccionado"] != DBNull.Value)
                        tmp.Seleccionado = (bool)dr["Seleccionado"];

                if (dr.Table.Columns.Contains("ValorSeleccionID"))
                    if (dr["ValorSeleccionID"] != DBNull.Value)
                        tmp.ValorSeleccionID = Convert.ToInt32(dr["ValorSeleccionID"]);

                tmp.Tipo = (dr["Tipo"] != DBNull.Value ? Convert.ToBoolean(dr["Tipo"]) : false);

                if (dr.Table.Columns.Contains("Seleccionado") && dr.Table.Columns.Contains("Tipo") && tmp.Seleccionado != null)
                {
                    bool tipo = (bool)tmp.Tipo;
                    bool seleccionado = (bool)tmp.Seleccionado;

                    if ((tipo && seleccionado) || (!tipo && !seleccionado))
                        tmp.EsCorrecta = true;
                    else
                        tmp.EsCorrecta = false;


                }
                else
                    tmp.EsCorrecta = null;
            }

            return tmp;
        }
    }

    public class ValorEnunciados
    {
        public Valor Valor { get; set; }
        public List<Enunciado> Enunciados { get; set; }
    }
}

