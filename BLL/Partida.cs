﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Partida
    {
        public int ID { get; set; }
        public String Nombre { get; set; }
        public int CantidadTrimestres { get; set; }
        public int AdministradorID { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public bool Estado { get; set; }
        public bool? Aceptada { get; set; }

        public static Partida ObtenerPorID(int id)
        {
            DataTable dt = DataAccess.GetData("ObtenerPartidaPorID", id);
            return (dt.Rows.Count > 0 ? (Partida)dt.Rows[0] : null);
        }

        public static IEnumerable<Partida> ObtenerPartidas()
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<Partida> ObtenerPorUsuarioID(int usuarioID)
        {
            DataTable dt = DataAccess.GetData("ObtenerPartidasPorUsuarioID", usuarioID, "UsuarioID");

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Partida)dr;
                }
            }
        }

        public static Partida CrearPartida(Partida partida)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("Nombre", partida.Nombre));
            paramList.Add(new SqlParameter("CantidadTrimestres", partida.CantidadTrimestres));
            paramList.Add(new SqlParameter("AdministradorID", partida.AdministradorID));
            paramList.Add(new SqlParameter("Estado", partida.Estado));

            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("CrearPartida", paramList);
            }
            catch
            {
                throw;
            }

            return ((dt != null && dt.Rows.Count > 0) ? (Partida)dt.Rows[0] : null);
        }

        public static explicit operator Partida(DataRow dr)
        {
            Partida tmp = null;

            if (dr != null)
            {
                tmp = new Partida();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.Nombre = dr["Nombre"].ToString();
                tmp.CantidadTrimestres = Convert.ToInt32(dr["CantidadTrimestres"]);
                tmp.AdministradorID = Convert.ToInt32(dr["AdministradorID"]);

                if (dr["FechaInicio"] != DBNull.Value)
                {
                    tmp.FechaInicio = Convert.ToDateTime(dr["FechaInicio"]);
                }
                if (dr["FechaFinal"] != DBNull.Value)
                {
                    tmp.FechaFinal = Convert.ToDateTime(dr["FechaFinal"]);
                }

                tmp.Estado = (dr["Estado"] != DBNull.Value ? Convert.ToBoolean(dr["Estado"]) : false);


                if (dr.Table.Columns.Contains("Aceptada"))
                {
                    if (dr["Aceptada"] == DBNull.Value)
                        tmp.Aceptada = null;
                    else
                        tmp.Aceptada = (bool)dr["Aceptada"];
                }
            }

            return tmp;
        }

        public static void FinalizarPartida(int p)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("PartidaID", p));

            DataAccess.ExecuteScalar("PartidaFinalizar", param);
        }
    }
}
