﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BLL
{
    public class ValorSeleccion
    {
        public int ID { get; set; }
        public int PartidaID { get; set; }
        public int UsuarioID { get; set; }
        public int EnunciadoID { get; set; }
        public int ValorID { get; set; }
        public bool? Seleccionado { get; set; }
        public int Trimestre { get; set; }
        public bool? Tipo { get; set; }

        public static int CrearValorSeleccion(int partidaId, int usuarioId, int enunciadoId, int valorId, int Trimestre)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", partidaId));
            paramList.Add(new SqlParameter("UsuarioID", usuarioId));
            paramList.Add(new SqlParameter("EnunciadoID", enunciadoId));
            paramList.Add(new SqlParameter("ValorID", valorId));
            paramList.Add(new SqlParameter("Trimestre", Trimestre));

            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("CrearValoresSeleccion", paramList);
            }
            catch
            {
                throw;
            }

            return Convert.ToInt32(dt.Rows[0]["ID"]);

        }

        public static bool Modificar(int id, bool value)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("ID", id));
            paramList.Add(new SqlParameter("Seleccionado", value));

            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("ActualizarValoresSeleccion", paramList);
            }
            catch
            {
                throw;
            }

            return (dt != null && dt.Rows.Count > 0);
        }


        public static IEnumerable<ValorSeleccion> ObtenerValoresSeleccionPorPartidaIDYUsuarioID(int partidaId, int usuarioId)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", partidaId));
            paramList.Add(new SqlParameter("UsuarioID", usuarioId));


            DataTable dt = DataAccess.GetData("ObtenerValoresSeleccionPorPartidaIDYUsuarioID", paramList);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (ValorSeleccion)dr;
                }
            }
        }


        public static explicit operator ValorSeleccion(DataRow dr)
        {
            ValorSeleccion tmp = null;

            if (dr != null)
            {
                tmp = new ValorSeleccion();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.PartidaID = Convert.ToInt32(dr["PartidaID"]);
                tmp.UsuarioID = Convert.ToInt32(dr["UsuarioID"]);
                tmp.EnunciadoID = Convert.ToInt32(dr["EnunciadoID"]);
                tmp.ValorID = Convert.ToInt32(dr["ValorID"]);
                tmp.Trimestre = Convert.ToInt32(dr["Trimestre"]);

                if (dr.Table.Columns.Contains("Tipo"))
                {
                    if (dr["Tipo"] != DBNull.Value)
                    {
                        tmp.Tipo = (bool)dr["Tipo"];
                    }
                }
                else
                    tmp.Tipo = null;

                if (dr["Seleccionado"] != DBNull.Value)
                    tmp.Seleccionado = (bool)dr["Seleccionado"];
                else
                    tmp.Seleccionado = null;
            }

            return tmp;
        }
    }

    public class ValorSeleccionado
    {
        public int ID { get; set; }
        public bool Value { get; set; }
    }
}
