﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    [Serializable]
    public class Role
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; }

        public static Role ObtenerPorID(int roleID)
        {
            DataTable dt = DataAccess.GetData("ObtenerRolePorID", roleID);
            return ((dt != null && dt.Rows.Count > 0) ? (Role)dt.Rows[0] : null);
        }

        public static Role ObtenerPorNombre(string Nombre)
        {
            DataTable dt = DataAccess.GetData("ObtenerRolPorNombre", Nombre, "Nombre");
            return ((dt != null && dt.Rows.Count > 0) ? (Role)dt.Rows[0] : null);
        }

        public static IEnumerable<Role> ObtenerTodos()
        {
            DataTable dt = DataAccess.GetData("ObtenerRoles");

            if (dt != null && dt.Rows.Count > 0)
            { 
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Role)dr;
                }
            }
        }

        public static explicit operator Role(DataRow dr)
        {
            Role tmp = null;

            if (dr != null)
            {
                tmp = new Role();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.Nombre = dr["Nombre"].ToString();
                tmp.Estado = (dr["Estado"] != DBNull.Value ? Convert.ToBoolean(dr["Estado"]) : false);
            }

            return tmp;
        }
    }
}
