﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace BLL
{
    public class Cryptology
    {
        public static string EncryptPassword(string plainPassword)
        {
            try
            {
                return BCrypt.Net.BCrypt.HashPassword(plainPassword, 13);
            }
            catch
            {
                throw;
            }
        }

        public static bool Verify(string plain, string hashed)
        {
            try
            {
                return BCrypt.Net.BCrypt.Verify(plain, hashed);
            }
            catch
            {
                return false;
            }
        }

        public static string GenerateRandomCode(int length = 100)
        {
            string charPool = "ABCDEFGOPQRSTUVWXY1234567890ZabcdefghijklmHIJKLMNnopqrstuvwxyz";
            StringBuilder rs = new StringBuilder();
            Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                rs.Append(charPool[(int)(random.NextDouble() * charPool.Length)]);
            }
            return rs.ToString();
        }
    }
}
