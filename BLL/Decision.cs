﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Decision
    {
        public int ID { get; set; }
        public int PartidaID { get; set; }
        public int BancoID { get; set; }
        public int Trimestre { get; set; }
        public decimal TasaCredito { get; set; }
        public decimal TasaPrestamo { get; set; }
        public decimal TasaAhorro { get; set; }
        public decimal TasaCertificado { get; set; }
        public decimal Mercadeo { get; set; }
        public decimal InvestigacionDesarrollo { get; set; }
        public decimal ResponsabilidadSocial { get; set; }
        public Indicador Indicador { get; set; }
        public bool HaDecidido { get; set; }

        public Decision()
        { }

        public Decision(int PartidaID, int BancoID, int Trimestre, decimal TasaCredito, decimal TasaPrestamo, decimal TasaAhorro, decimal TasaCertificado, decimal Mercadeo, decimal InvestigacionDesarrollo, decimal ResponsabilidadSocial)
        {
            this.PartidaID = PartidaID;
            this.BancoID = BancoID;
            this.Trimestre = Trimestre;
            this.TasaCredito = TasaCredito;
            this.TasaPrestamo = TasaPrestamo;
            this.TasaAhorro = TasaAhorro;
            this.TasaCertificado = TasaCertificado;
            this.Mercadeo = Mercadeo;
            this.InvestigacionDesarrollo = InvestigacionDesarrollo;
            this.ResponsabilidadSocial = ResponsabilidadSocial;
        }

        public static Decision ObtenerPorID(int ID)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<Decision> ObtenerPorPartida(int PartidaID)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<Decision> ObtenerDecisionesPorPartidaYBanco(int PartidaID, int BancoID)
        {
            //ObtenerDecisionesPorPartidaYUsuario
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", PartidaID));
            paramList.Add(new SqlParameter("BancoID", BancoID));
            DataTable dt = DataAccess.GetData("ObtenerDecisionesPorPartidaYBanco", paramList);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Decision)dr;
                }
            }
        }

        public static IEnumerable<Decision> ObtenerPorPartidaYUsuarioYTrimestre(int PartidaID, int UsuarioID, int Trimestre)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<Decision> ObtenerPorPartidaYTrimestre(int PartidaID, int Trimestre)
        {
            throw new NotImplementedException();
        }

        public static Decision CrearDecision(Decision decision)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", decision.PartidaID));
            paramList.Add(new SqlParameter("BancoID", decision.BancoID));
            paramList.Add(new SqlParameter("Trimestre", decision.Trimestre));
            paramList.Add(new SqlParameter("TasaCredito", decision.TasaCredito));
            paramList.Add(new SqlParameter("TasaPrestamo", decision.TasaPrestamo));
            paramList.Add(new SqlParameter("TasaAhorro", decision.TasaAhorro));
            paramList.Add(new SqlParameter("TasaCertificado", decision.TasaCertificado));
            paramList.Add(new SqlParameter("Mercadeo", decision.Mercadeo));
            paramList.Add(new SqlParameter("InvestigacionDesarrollo", decision.InvestigacionDesarrollo));
            paramList.Add(new SqlParameter("ResponsabilidadSocial", decision.ResponsabilidadSocial));



            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("CrearDecision", paramList);
            }
            catch
            {
                throw;
            }

            return ((dt != null && dt.Rows.Count > 0) ? (Decision)dt.Rows[0] : null);
        }

        public static explicit operator Decision(DataRow dr)
        {
            Decision tmp = null;

            if (dr != null)
            {
                tmp = new Decision();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.PartidaID = Convert.ToInt32(dr["PartidaID"]);
                tmp.BancoID = Convert.ToInt32(dr["BancoID"]);
                tmp.Trimestre = Convert.ToInt32(dr["Trimestre"]);
                tmp.TasaCredito = Convert.ToDecimal(dr["TasaCredito"]);
                tmp.TasaPrestamo = Convert.ToDecimal(dr["TasaPrestamo"]);
                tmp.TasaAhorro = Convert.ToDecimal(dr["TasaAhorro"]);
                tmp.TasaCertificado = Convert.ToDecimal(dr["TasaCertificado"]);
                tmp.Mercadeo = Convert.ToDecimal(dr["Mercadeo"]);
                tmp.InvestigacionDesarrollo = Convert.ToDecimal(dr["InvestigacionDesarrollo"]);
                tmp.ResponsabilidadSocial = Convert.ToDecimal(dr["ResponsabilidadSocial"]);

                tmp.Indicador = Indicador.ObtenerIndicadorPorTrimestreYBancoIDYPartidaID(tmp.Trimestre, tmp.BancoID, tmp.PartidaID);
            }

            return tmp;
        }

        /// <summary>
        /// Indica si todos los usuarios han tomado la decision del
        /// trimestre indicado.
        /// </summary>
        /// <param name="Trimestre">El # del trimestre que se desea verificar.</param>
        /// <returns>True/False si han tomado o no las decisiones de este trimestre.</returns>
        public static bool HanTomadoDecisiones(int PartidaID, int Trimestre)
        {
            return Usuario.ObtenerBancosYUsuariosFaltantePorPartidaYTrimestre(PartidaID, Trimestre).Count() <= 0;
        }
    }
}

