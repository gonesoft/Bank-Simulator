﻿
namespace BLL
{
    public class ClaseSocial
    {
        public enum Tipo
        {
            Alta,
            Media,
            Baja
        }

        public static int Length()
        {
            return System.Enum.GetNames(typeof(ClaseSocial.Tipo)).Length;
        }
    }

}
