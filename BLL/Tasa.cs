﻿
namespace BLL
{
    public class Tasa
    {
        public enum Tipo
        {
            Tasa_Linea_Credito,
            Tasa_Prestamo,
            Tasa_Ahorro,
            Tasa_Certificado
        }

        public static int Length()
        {
            return System.Enum.GetNames(typeof(Tasa.Tipo)).Length;
        }
    }
}
