﻿
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace BLL
{
    public class Indicador : ICloneable
    {
        public int ID { get; set; }
        public int BancoID { get; set; }
        public int PartidaID { get; set; }
        public int Trimestre { get; set; }
        public decimal CapacidadCrediticia { get; set; }
        public decimal CreditosPendientes { get; set; }
        public decimal DepositosPendientes { get; set; }
        public decimal ReservasCapital { get; set; }
        public decimal PrestamoOperaciones { get; set; }
        public decimal DepositosOperaciones { get; set; }
        public decimal CapitalTrabajo { get; set; }
        

        //TODO: Cargar desde la base de datos
        public static Indicador ObtenerPorDefecto()
        {
            Indicador tmp = new Indicador();
            tmp.CapitalTrabajo = 10000000;
            tmp.CapacidadCrediticia = 4500000;
            tmp.CreditosPendientes = 0;
            tmp.DepositosPendientes = 0;
            tmp.ReservasCapital = 1500000;
            tmp.PrestamoOperaciones = 0;
            tmp.DepositosOperaciones = 0;

            tmp.CapitalTrabajo -= (tmp.CapacidadCrediticia + tmp.ReservasCapital);
            return tmp;
        }

        public static Indicador ObtenerIndicadorPorTrimestreYBancoID(int Trimestre, int BancoID)
        {
            //ObtenerDecisionesPorPartidaYUsuario
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("Trimestre", Trimestre));
            paramList.Add(new SqlParameter("BancoID", BancoID));
            DataTable dt = DataAccess.GetData("ObtenerIndicadorPorTrimestreYBancoID", paramList);
            return ((dt != null && dt.Rows.Count > 0) ? (Indicador)dt.Rows[0] : null);
        }

        public static Indicador ObtenerIndicadorPorTrimestreYBancoIDYPartidaID(int Trimestre, int BancoID, int PartidaID)
        {
            //ObtenerDecisionesPorPartidaYUsuario
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("Trimestre", Trimestre));
            paramList.Add(new SqlParameter("BancoID", BancoID));
            paramList.Add(new SqlParameter("PartidaID", PartidaID));
            DataTable dt = DataAccess.GetData("ObtenerIndicadorPorTrimestreYBancoIDYPartidaID", paramList);
            return ((dt != null && dt.Rows.Count > 0) ? (Indicador)dt.Rows[0] : null);
        }

        public static IEnumerable<Indicador> ObtenerIndicadoresPorBancoIDYPartidaID(int BancoID, int PartidaID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", PartidaID));
            paramList.Add(new SqlParameter("BancoID", BancoID));

            DataTable dt = DataAccess.GetData("ObtenerIndicadoresPorBancoIDYPartidaID", paramList);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Indicador)dr;
                }
            }
        }

        public static IEnumerable<Indicador> ObtenerIndicadoresPorPartidaYTrimestre(int partidaId, int trimestre)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", partidaId));
            paramList.Add(new SqlParameter("Trimestre", trimestre));

            DataTable dt = DataAccess.GetData("ObtenerIndicadoresPorPartidaYTrimestre", paramList);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Indicador)dr;
                }
            }
        }

        public static Indicador CrearIndicador(Indicador indicador)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("BancoID", indicador.BancoID));
            paramList.Add(new SqlParameter("PartidaID", indicador.PartidaID));
            paramList.Add(new SqlParameter("Trimestre", indicador.Trimestre));
            paramList.Add(new SqlParameter("CapitalTrabajo", indicador.CapitalTrabajo));
            paramList.Add(new SqlParameter("CapacidadCrediticia", indicador.CapacidadCrediticia));
            paramList.Add(new SqlParameter("CreditosPendientes", indicador.CreditosPendientes));
            paramList.Add(new SqlParameter("DepositosPendientes", indicador.DepositosPendientes));
            paramList.Add(new SqlParameter("ReservasCapital", indicador.ReservasCapital));
            paramList.Add(new SqlParameter("PrestamoOperaciones", indicador.PrestamoOperaciones));
            paramList.Add(new SqlParameter("DepositosOperaciones", indicador.DepositosOperaciones));

            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("CrearIndicador", paramList);
            }
            catch
            {
                throw;
            }

            return ((dt != null && dt.Rows.Count > 0) ? (Indicador)dt.Rows[0] : null);
        }

        public object Clone()
        {
            Indicador indicador = new Indicador();
            indicador.CapitalTrabajo = CapitalTrabajo;
            indicador.CapacidadCrediticia = CapacidadCrediticia;
            indicador.CreditosPendientes = CreditosPendientes;
            indicador.DepositosPendientes = DepositosPendientes;
            indicador.ReservasCapital = ReservasCapital;
            indicador.PrestamoOperaciones = PrestamoOperaciones;
            indicador.DepositosOperaciones = DepositosOperaciones;
            indicador.Trimestre = Trimestre;
            indicador.ID = ID;
            indicador.BancoID = BancoID;
            indicador.PartidaID = PartidaID;
            return indicador;
        }

        public static explicit operator Indicador(DataRow dr)
        {
            Indicador tmp = null;

            if (dr != null)
            {
                tmp = new Indicador();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.BancoID = Convert.ToInt32(dr["BancoID"]);
                tmp.PartidaID = Convert.ToInt32(dr["PartidaID"]);
                tmp.Trimestre = Convert.ToInt32(dr["Trimestre"]);
                tmp.CapitalTrabajo = Convert.ToDecimal(dr["CapitalTrabajo"]);
                tmp.CapacidadCrediticia = Convert.ToDecimal(dr["CapacidadCrediticia"]);
                tmp.CreditosPendientes = Convert.ToDecimal(dr["CreditosPendientes"]);
                tmp.DepositosPendientes = Convert.ToDecimal(dr["DepositosPendientes"]);
                tmp.ReservasCapital = Convert.ToDecimal(dr["ReservasCapital"]);
                tmp.PrestamoOperaciones = Convert.ToDecimal(dr["PrestamoOperaciones"]);
                tmp.DepositosOperaciones = Convert.ToDecimal(dr["DepositosOperaciones"]);
            }

            return tmp;
        }

        public decimal obtenerCapital()
        {
            return this.CapitalTrabajo + this.CapacidadCrediticia + this.ReservasCapital - this.CreditosPendientes;
        }
    }
}
