﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class NumberHelper
    {
        /// <summary>
        /// Convierte un numero int a su correspondiente string ordinal.
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static string AOrdinal(int n, bool s = false)
        {
            string tmp = "";
            switch (n)
            { 
                case 0:
                    tmp = "Cero";
                    break;
                case 1:
                    tmp = "Primer" + (s ? "a" : "");
                    break;
                case 2:
                    tmp = "Segund" + (s ? "a" : "o");
                    break;
                case 3:
                    tmp = "Tercer" + (s ? "a" : "");
                    break;
                case 4:
                    tmp = "Cuart" + (s ? "a" : "o");
                    break;
                case 5:
                    tmp = "Quint" + (s ? "a" : "o");
                    break;
                case 6:
                    tmp = "Sext" + (s ? "a" : "o");
                    break;
                case 7:
                    tmp = "Septim" + (s ? "a" : "o");
                    break;
                case 8:
                    tmp = "Octav" + (s ? "a" : "o");
                    break;
                case 9:
                    tmp = "Noven" + (s ? "a" : "o");
                    break;
                case 10:
                    tmp = "Decim" + (s ? "a" : "o");
                    break;
            }
            return tmp;
        }
    }
}
