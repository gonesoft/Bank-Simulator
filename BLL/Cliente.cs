﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Cliente
    {
        public NivelSocial NivelSocial { get; set; }
        public List<RangoInversion> Negocios { get; set; }
        public Cliente(NivelSocial pNivelSocial)
        {
            NivelSocial = pNivelSocial;
            Negocios = new List<RangoInversion>();
        }



        public void GenerarInversiones()
        {
            List<double> tempInversiones = new List<double>();
            Random rmd = new Random();
            foreach (RangoInversion ri in Negocios)
            {
                ri.Accion = rmd.Next((int)ri.Desde, (int)ri.Hasta);
            }
        }

        /// <summary>
        /// Genera un numero "cantidad" de clientes, y les asigna una clase
        /// social aleatoria, al igual que un numero aleatorio de Tasa, o tipo
        /// de compras que realizara.
        /// </summary>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        public static List<Cliente> GenerarClientes(int cantidad, int inversionesMaximas = 1)
        {
            Random rmd = new Random();
            List<Cliente> temp = null;
            Cliente clienteTemp = null;
            if (cantidad > 0)
            {
                temp = new List<Cliente>();

                for (int i = 0; i < cantidad; i++)
                {
                    //asigna un nivel social aleatorio, al igual que una cantidad aleatoria
                    //de compras
                    NivelSocial ns = new NivelSocial();
                    ns.TipoClaseSocial = (ClaseSocial.Tipo)rmd.Next(ClaseSocial.Length());

                    clienteTemp = new Cliente(ns);

                    int inversiones = rmd.Next(0, inversionesMaximas + 1);
                    for (int x = 0; x < inversiones; x++)
                    {
                        RangoInversion ri = RangoInversion.ObtenerPorClaseSocialYTasa(ns.TipoClaseSocial, (Tasa.Tipo)rmd.Next(Tasa.Length()));
                        clienteTemp.Negocios.Add(ri);
                    }


                    temp.Add(clienteTemp);
                }
            }

            return temp;

        }
    }
}
