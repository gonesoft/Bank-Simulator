﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Banco
    {
        #region "MIEMBROS/PROPIEDADES"

        public int ID { get; set; }
        public string Nombre { get; set; }
        public Usuario Usuario { get; set; }
        public DateTime UltimaActualizacion { get; set; }
        public bool Estado { get; set; }

        public List<Decision> Decisiones { get; set; }
        public List<Cliente> ClientesPotenciales { get; set; }
        public List<Cliente> ClientesPendientes { get; set; }
        public List<Cliente> Clientes { get; set; }

        #endregion

        #region "CONSTRUCTORES"

        public Banco(int _id, string _nombre, int _usuarioId, DateTime _ultimaActualizacion, bool _estado)
        {
            ID = _id;
            Nombre = _nombre;
            Usuario = Usuario.ObtenerUsuarioPorID(_usuarioId);
            UltimaActualizacion = _ultimaActualizacion;
            Estado = _estado;
        }

        public Banco()
        {
            ClientesPendientes = new List<Cliente>();
        }

        public Banco(int trimestres)
        {
            Decisiones = new List<Decision>();
            ClientesPotenciales = new List<Cliente>();
            Clientes = new List<Cliente>();
        }

        #endregion

        #region "METODOS PUBLICOS"

        public void GuardarDecision(int Trimestre)
        {
            //(3) Capacidad crediticia de todo el sistema bancario que depende 
            //del volumen de depósitos captados y las reservas que tienen los bancos. 
            //También aquí tenemos información acerca de la tasa de interés que paga 
            //el Banco Central por los depósitos que realizan los bancos cuando les 
            //sobran fondos y la tasa que cobra cuando presta dinero a los bancos que 
            //les faltan fondos.


            if (ClientesPendientes == null)
            {
                ClientesPendientes = new List<Cliente>();
            }


            Decision trimestreAnterior = this.Decisiones[(Trimestre <= 0 ? 0 : Trimestre - 1)];
            int cTrimestre = Trimestre;
            this.Decisiones[cTrimestre].Indicador = (Indicador)trimestreAnterior.Indicador.Clone();

            foreach (Cliente cliente in this.Clientes)
            {
                foreach (RangoInversion ri in cliente.Negocios)
                {
                    decimal diezPorciento = ri.Accion / 10;
                    switch (ri.TipoTasa)
                    {
                        case Tasa.Tipo.Tasa_Ahorro:

                            this.Decisiones[cTrimestre].Indicador.CapacidadCrediticia += (ri.Accion - diezPorciento);
                            this.Decisiones[cTrimestre].Indicador.ReservasCapital += diezPorciento;

                            //cantidad de dinero a pagar al cliente por sus ahorros en el trimestre
                            decimal pagoAhorro = ((ri.Accion * (this.Decisiones[cTrimestre].TasaAhorro / 100)) / 360) * 90;
                            //se la restamos a la reservas de captial
                            this.Decisiones[cTrimestre].Indicador.ReservasCapital -= pagoAhorro;

                            break;
                        case Tasa.Tipo.Tasa_Certificado:
                            this.Decisiones[cTrimestre].Indicador.CapacidadCrediticia += ri.Accion;
                            //cantidad que se le pagara a este cliente en el trimestre
                            decimal pagoCertificado = ((ri.Accion * (this.Decisiones[cTrimestre].TasaCertificado / 100)) / 360) * 90;
                            //se la restamos a la reservas de capital
                            this.Decisiones[cTrimestre].Indicador.ReservasCapital -= pagoCertificado;

                            break;
                        case Tasa.Tipo.Tasa_Linea_Credito:
                            this.Decisiones[cTrimestre].Indicador.CapacidadCrediticia -= ri.Accion;
                            this.Decisiones[cTrimestre].Indicador.CreditosPendientes -= ri.Accion;

                            //cantidad de dinero obtenida por los intereses
                            decimal gananciaCredito = (ri.Accion * (this.Decisiones[cTrimestre].TasaCredito / 100) / 360) * 90;
                            //se le aumentan a la reservas de capital
                            this.Decisiones[cTrimestre].Indicador.ReservasCapital += gananciaCredito;
                            ClientesPendientes.Add(cliente);
                            break;
                        case Tasa.Tipo.Tasa_Prestamo:
                            this.Decisiones[cTrimestre].Indicador.CapacidadCrediticia -= ri.Accion;
                            this.Decisiones[cTrimestre].Indicador.CreditosPendientes -= ri.Accion;

                            //cantidad de dinero obtenida por los intereses
                            decimal gananciaPrestamo = (ri.Accion * (this.Decisiones[cTrimestre].TasaPrestamo / 100) / 360) * 90;
                            //se le aumentan a la reservas de capital
                            this.Decisiones[cTrimestre].Indicador.ReservasCapital += gananciaPrestamo;

                            ClientesPendientes.Add(cliente);
                            break;
                    }
                }
            }

            //guardamos el indicador
            this.Decisiones[cTrimestre].Indicador.Trimestre = cTrimestre;
            this.Decisiones[cTrimestre].Indicador.CapitalTrabajo -= (this.Decisiones[cTrimestre].InvestigacionDesarrollo + this.Decisiones[cTrimestre].ResponsabilidadSocial + this.Decisiones[cTrimestre].Mercadeo);
            this.Decisiones[cTrimestre].Indicador = Indicador.CrearIndicador(this.Decisiones[cTrimestre].Indicador);

            if (this.Decisiones[cTrimestre].Indicador == null)
            { 
                //TODO: throw an actual error here
                throw new NotImplementedException();
            }

            //calculos para creditos pendientes y depositos pendientes
            //cada cliente tiene un rango de 75 a 90% de posibilidades
            //de pagar sus deudas en el trimestre, si no paga, se agrega
            //a la lista de ClientesPendientes, y se calcula el proximo
            //trimestre de esta lista
            Random rnd = new Random();
            int take = (int)(ClientesPendientes.Count * .85);
            List<Cliente> pagaran = ClientesPendientes.OrderBy(x => rnd.Next()).Take(take).ToList();

            foreach (Cliente cPaga in pagaran)
            {
                foreach (RangoInversion ri in cPaga.Negocios)
                {
                    this.Decisiones[cTrimestre].Indicador.CreditosPendientes += ri.Accion;
                    this.Decisiones[cTrimestre].Indicador.CapacidadCrediticia += ri.Accion;
                    //this.Trimestres[cTrimestre].Indicador.DepositosPendientes -= ri.Accion;
                }
                ClientesPendientes.Remove(cPaga);
            }

            foreach (Cliente cDebe in ClientesPendientes)
            {
                foreach (RangoInversion ri in cDebe.Negocios)
                {
                    this.Decisiones[cTrimestre].Indicador.CreditosPendientes -= ri.Accion;
                }
            }
        }

        #endregion

        #region "METODOS ESTATICOS"
        public static Banco ObtenerPorUsuario(int UsuarioID)
        {
            DataTable dt = DataAccess.GetData("ObtenerBancoPorUsuario", UsuarioID, "UsuarioID");

            return ((dt != null && dt.Rows.Count > 0) ? (Banco)dt.Rows[0] : null);
        }

        public static Banco ObtenerPorUsuarioYPartidaID(int UsuarioID, int PartidaID)
        { 
         DataTable dt = DataAccess.GetData("ObtenerBancoPorUsuario", UsuarioID, "UsuarioID");
            Banco tmp = ((dt != null && dt.Rows.Count > 0) ? (Banco)dt.Rows[0] : null);

            if (tmp != null)
            {
                tmp.Decisiones = Decision.ObtenerDecisionesPorPartidaYBanco(PartidaID, tmp.ID).ToList();
            }

            return tmp;
        }

        public static IEnumerable<Banco> ObtenerBancosPorPartidaID(int PartidaID)
        {
            DataTable dt = DataAccess.GetData("ObtenerBancosPorPartidaID", PartidaID, "PartidaID");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Banco)dr;
                }
            }
        }

        public static List<Banco> ObtenerBancosConDecisionesPorPartidaID(int PartidaID)
        {
            DataTable dt = DataAccess.GetData("ObtenerBancosPorPartidaID", PartidaID, "PartidaID");

            List<Banco> tmp = null;

            if (dt != null && dt.Rows.Count > 0)
            {
                tmp = new List<Banco>();

                foreach (DataRow dr in dt.Rows)
                {
                    Banco b = (Banco)dr;
                    b.Decisiones = Decision.ObtenerDecisionesPorPartidaYBanco(PartidaID, b.ID).ToList();
                    tmp.Add(b);
                }
            }

            return tmp;
        }

        public static void AsignarClientesPorTrimestre(List<Banco> bancos, int trimestre)
        {

            Random rmd = new Random();
            List<Cliente> clientesSinBancos = new List<Cliente>();
            foreach (Banco b in bancos)
            {
                foreach (Cliente c in b.ClientesPotenciales)
                {
                    foreach (RangoInversion ri in c.Negocios)
                    {
                        Banco conveniente = ObtenerConveniente(bancos, ri.TipoTasa, trimestre);
                        //ya que tenemos el banco conveniente, decidir al hazar 
                        //si se queda en el banco o no
                        bool seQueda = rmd.Next(2) == 0;

                        c.GenerarInversiones();

                        if (seQueda)
                        {
                            //agregar como cliente
                            b.Clientes.Add(c);
                        }
                        else
                        {
                            clientesSinBancos.Add(c);
                        }
                    }
                }
            }

            //asignamos los clientes que se quedaron sin bancos, a bancos
            //al azar
            if (clientesSinBancos.Count > 0)
            {
                int cs = 0;
                foreach (Cliente c in clientesSinBancos)
                {
                    cs = rmd.Next(bancos.Count);
                    bancos[cs].Clientes.Add(c);

                }
            }
        }

        /// <summary>
        /// Decide que banco tiene la menor taza que le conviene al cliente en base a sus transacciones.
        /// </summary>
        /// <param name="bancos">Lista de bancos en la partida</param>
        /// <param name="tipoTasa">Tipo de tasa en la que estamos interesados</param>
        /// <param name="trimestre">Trimestre.</param>
        /// <returns>Devuelve el banco con la menor tasa.</returns>
        public static Banco ObtenerConveniente(List<Banco> bancos, Tasa.Tipo tipoTasa, int trimestre)
        {
            Banco bancoMenor = null;

            switch (tipoTasa)
            {
                case Tasa.Tipo.Tasa_Ahorro:
                    bancoMenor = bancos.OrderByDescending(x => x.Decisiones[trimestre].TasaAhorro).FirstOrDefault();
                    break;
                case Tasa.Tipo.Tasa_Certificado:
                    bancoMenor = bancos.OrderByDescending(x => x.Decisiones[trimestre].TasaCertificado).FirstOrDefault();
                    break;
                case Tasa.Tipo.Tasa_Linea_Credito:
                    bancoMenor = bancos.OrderBy(x => x.Decisiones[trimestre].TasaCredito).FirstOrDefault();
                    break;
                case Tasa.Tipo.Tasa_Prestamo:
                    bancoMenor = bancos.OrderBy(x => x.Decisiones[trimestre].TasaPrestamo).FirstOrDefault();
                    break;
            }
            return bancoMenor;
        }

        public static bool BancosHanDecidido(Jugada Jugada, int Trimestre)
        {
            if (Jugada != null && Jugada.Bancos != null && Jugada.Bancos.Count > 0)
            {
                foreach (Banco banco in Jugada.Bancos)
                {
                    if (banco.Decisiones[Trimestre] == null || !banco.Decisiones[Trimestre].HaDecidido)
                    {
                        //solo toma un banco sin tomar decisiones, para saber que no todos
                        //los bancos han tomado decisiones
                        return false;
                    }
                }
            }

            //todos han tomado decisiones en este trimestre
            return true;
        }

        public static void AsignarClientesPotencialesPorTrimestre(List<Banco> bancos, List<Cliente> clientes, int trimestre)
        {
            int numClientes = 0;
            int indiceComenzar = 0;
            Random rmd = new Random();
            Banco mayorInversion = null;
            int asignaraMayor = 0;
            Cliente c = null;

            while (true)
            {
                if (numClientes == 0)
                {
                    numClientes = clientes.Count;
                }
                else
                {
                    numClientes = clientes.Count - indiceComenzar;
                }

                if (numClientes <= 1)
                    break;

                //selecciona el banco que invierte mas dinero en I&D, Mercadeo y Responsabilidad Social
                mayorInversion = bancos.Where(x => x.ClientesPotenciales.Count == 0).OrderByDescending(x => x.Decisiones[trimestre].InvestigacionDesarrollo + x.Decisiones[trimestre].Mercadeo + x.Decisiones[trimestre].ResponsabilidadSocial).FirstOrDefault();
                //51% de los clientes seran potencialmente asignados
                //al banco con mayor inversion en las areas antes mencionadas
                asignaraMayor = Convert.ToInt32(numClientes * .51);

                if (mayorInversion != null)
                {
                    for (int i = indiceComenzar; i < (clientes.Count - asignaraMayor); i++)
                    {
                        c = clientes[i];
                        mayorInversion.ClientesPotenciales.Add(c);
                        indiceComenzar++;
                    }
                }
                else
                {
                    //asigna el resto de los clientes al azar
                    if (indiceComenzar <= clientes.Count)
                    {
                        int cs = rmd.Next(bancos.Count);
                        for (int i = indiceComenzar; i < clientes.Count; i++)
                        {
                            bancos[cs].ClientesPotenciales.Add(clientes[i]);
                        }
                    }
                    break;
                }
            }
        }

        public static Banco CrearBanco(string Nombre, int UsuarioID, bool Estado = true)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("Nombre", Nombre));
            paramList.Add(new SqlParameter("UsuarioID", UsuarioID));
            paramList.Add(new SqlParameter("Estado", Estado));
            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("CrearBanco", paramList);
            }
            catch
            {
                throw;
            }

            return ((dt != null && dt.Rows.Count > 0) ? (Banco)dt.Rows[0] : null);

        }

        #endregion

        #region "OPERADORES"

        public static explicit operator Banco(DataRow dr)
        {
            Banco tmp = null;

            if (dr != null)
            {
                tmp = new Banco();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.Nombre = dr["Nombre"].ToString();
                int usuarioID = Convert.ToInt32(dr["UsuarioID"]);
                tmp.UltimaActualizacion = Convert.ToDateTime(dr["UltimaActualizacion"]);
                tmp.Estado = (dr["Estado"] != DBNull.Value ? Convert.ToBoolean(dr["Estado"]) : false);
                tmp.Usuario = Usuario.ObtenerUsuarioPorID(usuarioID);
                //TODO: Cargar la lista de trimestres de este banco


                if (usuarioID > 0)
                {
                    tmp.Usuario = Usuario.ObtenerUsuarioPorID(usuarioID);
                }
            }

            return tmp;
        }

        #endregion
    }
}
