﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class RangoInversion
    {

        public RangoInversion(decimal pDesde, decimal pHasta, Tasa.Tipo pTasa)
        {
            Desde = pDesde;
            Hasta = pHasta;
            TipoTasa = pTasa;
            Accion = 0;
        }

        public decimal Desde { get; set; }
        public decimal Hasta { get; set; }
        public decimal Accion { get; set; }

        public Tasa.Tipo TipoTasa { get; set; }

        public static RangoInversion ObtenerPorClaseSocialYTasa(ClaseSocial.Tipo claseSocial, Tasa.Tipo tasa)
        {
            RangoInversion temp = null;

            if (claseSocial == ClaseSocial.Tipo.Alta)
            {
                if (tasa == BLL.Tasa.Tipo.Tasa_Ahorro)
                {
                    temp = new RangoInversion(5000, 50000, tasa);
                }
                else if (tasa == BLL.Tasa.Tipo.Tasa_Certificado)
                {
                    temp = new RangoInversion(1000, 2000, tasa);
                }
                else if (tasa == BLL.Tasa.Tipo.Tasa_Linea_Credito)
                {
                    temp = new RangoInversion(2000, 10000, tasa);
                }
                else if ( tasa == BLL.Tasa.Tipo.Tasa_Prestamo)
                {
                    temp = new RangoInversion(20000, 100000, tasa);
                }
            }
            else if (claseSocial == ClaseSocial.Tipo.Media)
            {
                if (tasa == BLL.Tasa.Tipo.Tasa_Ahorro)
                {
                    temp = new RangoInversion(50, 100, tasa);
                }
                else if (tasa == BLL.Tasa.Tipo.Tasa_Certificado)
                {
                    temp = new RangoInversion(500, 1000, tasa);
                }
                else if (tasa == BLL.Tasa.Tipo.Tasa_Linea_Credito)
                {
                    temp = new RangoInversion(1000, 5000, tasa);
                }
                else if (tasa == BLL.Tasa.Tipo.Tasa_Prestamo)
                {
                    temp = new RangoInversion(10000, 50000, tasa);
                }
            }
            else if (claseSocial == ClaseSocial.Tipo.Baja)
            {
                if (tasa == BLL.Tasa.Tipo.Tasa_Ahorro)
                {
                    temp = new RangoInversion(500, 5000, tasa);
                }
                else if (tasa == BLL.Tasa.Tipo.Tasa_Certificado)
                {
                    temp = new RangoInversion(250, 500, tasa);
                }
                else if (tasa == BLL.Tasa.Tipo.Tasa_Linea_Credito)
                {
                    temp = new RangoInversion(5000, 10000, tasa);
                }
                else if (tasa == BLL.Tasa.Tipo.Tasa_Prestamo)
                {
                    temp = new RangoInversion(5000, 25000, tasa);
                }
            }

            return temp;
        }
    }
}
