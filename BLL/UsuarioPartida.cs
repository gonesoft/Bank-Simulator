﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BLL
{
    public class UsuarioPartida
    {
        public int ID { get; set; }
        public int UsuarioID { get; set; }
        public int PartidaID { get; set; }
        public Nullable<bool> Aceptada { get; set; }

        public UsuarioPartida()
        { }

        public UsuarioPartida(int _id, int _usuarioId, int _partidaId, Nullable<bool> _aceptada)
        {
            ID = _id;
            UsuarioID = _usuarioId;
            PartidaID = _partidaId;
            Aceptada = _aceptada;
        }

        public UsuarioPartida(int usuarioId, int partidaId, Nullable<bool> aceptada)
        {
            this.UsuarioID = usuarioId;
            this.PartidaID = partidaId;
            this.Aceptada = aceptada;
        }

        public static UsuarioPartida ObtenerUsuarioPartidaPorID(int usuarioID)
        {
            DataTable dt = DataAccess.GetData("ObtenerUsuarioPartidaPorID", usuarioID);
            return ((dt != null && dt.Rows.Count > 0) ? (UsuarioPartida)dt.Rows[0] : null);
        }

        public static UsuarioPartida ObtenerUsuarioPartidaPorPartidaIDYUsuarioID(int partidaID, int usuarioID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("UsuarioID", usuarioID));
            paramList.Add(new SqlParameter("PartidaID", partidaID));
            DataTable dt = DataAccess.GetData("ObtenerUsuarioPartidaPorPartidaIDYUsuarioID", paramList);
            return (dt.Rows.Count > 0 ? (UsuarioPartida)dt.Rows[0] : null);
        }

        public static IEnumerable<UsuarioPartida> ObtenerUsuariosPartidasPorPartidaID(int PartidaID)
        {
            DataTable dt = DataAccess.GetData("ObtenerUsuariosPartidasPorPartidaID", PartidaID, "PartidaID");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (UsuarioPartida)dr;
                }
            }
        }

        public static UsuarioPartida CrearUsuarioPartida(UsuarioPartida usuarioPartida)
        {
      
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("UsuarioID", usuarioPartida.UsuarioID));
            paramList.Add(new SqlParameter("PartidaID", usuarioPartida.PartidaID));
            
            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("CrearUsuarioPartida", paramList);
            }
            catch
            {
                throw;
            }

            return ((dt != null && dt.Rows.Count > 0) ? (UsuarioPartida)dt.Rows[0] : null);
        }

        public static UsuarioPartida Modificar(UsuarioPartida usuarioPartida)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("ID", usuarioPartida.ID));
            paramList.Add(new SqlParameter("UsuarioID", usuarioPartida.UsuarioID));
            paramList.Add(new SqlParameter("PartidaID", usuarioPartida.PartidaID));
            paramList.Add(new SqlParameter("Aceptada", usuarioPartida.Aceptada));

            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("ActualizarUsuarioPartida", paramList);
            }
            catch
            {
                throw;
            }

            return ((dt != null && dt.Rows.Count > 0) ? (UsuarioPartida)dt.Rows[0] : null);
        }

        public static bool HanTomadoAcciones(int PartidaID)
        {
            IEnumerable<UsuarioPartida> up = ObtenerUsuariosPartidasPorPartidaID(PartidaID);

            bool result = false;

            if (up != null && up.Count() > 0)
            {
                result = up.ToList().Find(p => p.Aceptada == null) == null;
            }

            return result;
        }

        public static explicit operator UsuarioPartida(DataRow dr)
        {
            UsuarioPartida tmp = null;

            if (dr != null)
            {
                tmp = new UsuarioPartida();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.UsuarioID = Convert.ToInt32(dr["UsuarioID"]);
                tmp.PartidaID = Convert.ToInt32(dr["PartidaID"]);

                if (dr["Aceptada"] == DBNull.Value)
                {
                    tmp.Aceptada = null;
                }
                else
                {
                    tmp.Aceptada = Convert.ToBoolean(dr["Aceptada"]);
                }
                
            }

            return tmp;
        }
    }
}
