﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Jugada
    {
        public List<Banco> Bancos { get; set; }
        public int Trimestres { get; set; }
        public int TrimestreActual { get; set; }
        public int PartidaID { get; set; }

        public Jugada(List<Banco> Bancos, int Trimestres, int partidaID)
        {
            this.Bancos = Bancos;
            this.Trimestres = Trimestres;
            this.TrimestreActual = 0;
            this.PartidaID = partidaID;
            foreach (Banco b in Bancos)
            {
                if (b.Decisiones == null)
                {
                    b.Decisiones = new List<Decision>();
                }
            }

        }

        //Comienza una partida nueva
        public void Comenzar()
        {

            Decision decision;
            //va banco por banco, y le asigna los indicadores por defecto del
            //trimestre 0, al igual que toma la decision del trimestre 0
            foreach (Banco b in Bancos)
            {
                //TODO: Obtener de la base de datos
                decision = new Decision(PartidaID, b.ID, 0, 11, new Decimal(13.95), new Decimal(1.0), 4, 0, 0, 0);
                decision = Decision.CrearDecision(decision);
                decision.Indicador = Indicador.ObtenerPorDefecto();
                decision.Indicador.BancoID = b.ID;
                decision.Indicador.PartidaID = PartidaID;
                decision.Indicador.Trimestre = 0;

                b.Decisiones.Add(decision);
            }

            foreach (Banco b in Bancos)
            {
                asignarClientes(0);
                TomarDecision(b, 0);
            }

        }

        public void TomarDecision(Banco banco, int Trimestre)
        {
            //En este metodo debemos ejecutar el siguiente proceso:
            //1) El banco que esta tomando decisiones en este momento


            if (banco.Decisiones[Trimestre].HaDecidido)
            {
                //salgo del metodo, ya tome la decision
            }
            else
            {
                //guardo las decisiones
                banco.GuardarDecision(Trimestre);
                //hacer calculos en base a decision
                banco.Decisiones[Trimestre].HaDecidido = true;

            }

            if (Banco.BancosHanDecidido(this, Trimestre))
            {
                this.TrimestreActual++;
                //hacer calculos de indicadores
            }
        }

        public static void Decidir(Banco banco, int Trimestre)
        {
            if (!banco.Decisiones[Trimestre].HaDecidido)
            {
                banco.GuardarDecision(Trimestre);
                banco.Decisiones[Trimestre].HaDecidido = true;
            }
        }

        private void asignarClientes(int Trimestre)
        {
            Random rmd = new Random();
            //generar clientes basandonos en la cantidad de bancos

            //generamos una piscina (pool) de clientes, en base a la cantidad de bancos que juegan
            //ejemplo: si juegan dos bancos, la cantidad de clientes generados va en un rango de:
            //2 * 100 = 200 hasta 2 * 1000 = 2000
            List<Cliente> clientes = Cliente.GenerarClientes(rmd.Next((Bancos.Count * 10), (Bancos.Count * 50)), 1);

            Bancos.ForEach(x => x.ClientesPotenciales = new List<Cliente>());
            //asignamos clientes potenciales en este trimestre en base a los clientes generados
            Banco.AsignarClientesPotencialesPorTrimestre(Bancos, clientes, Trimestre);

            //asigna aleatoriamente clientes a un banco de el pool de clientes potenciales
            Bancos.ForEach(x => x.Clientes = new List<Cliente>());

            Banco.AsignarClientesPorTrimestre(Bancos, Trimestre);

        }

        public static void asignarClientes(int Trimestre, List<Banco> Bancos)
        {
            Random rmd = new Random();
            //generar clientes basandonos en la cantidad de bancos

            //generamos una piscina (pool) de clientes, en base a la cantidad de bancos que juegan
            //ejemplo: si juegan dos bancos, la cantidad de clientes generados va en un rango de:
            //2 * 100 = 200 hasta 2 * 1000 = 2000
            List<Cliente> clientes = Cliente.GenerarClientes(rmd.Next((Bancos.Count * 10), (Bancos.Count * 50)), 1);

            Bancos.ForEach(x => x.ClientesPotenciales = new List<Cliente>());
            //asignamos clientes potenciales en este trimestre en base a los clientes generados
            Banco.AsignarClientesPotencialesPorTrimestre(Bancos, clientes, Trimestre);

            //asigna aleatoriamente clientes a un banco de el pool de clientes potenciales
            Bancos.ForEach(x => x.Clientes = new List<Cliente>());

            Banco.AsignarClientesPorTrimestre(Bancos, Trimestre);
        }

        public static int ObtenerTrimestreActualPorPartidaID(int PartidaID)
        {
            DataTable dt = DAL.DataAccess.GetData("ObtenerTrimestreActualPorPartidaID", PartidaID, "PartidaID");
            int tmp = -1;

            if (dt != null && dt.Rows.Count > 0)
            {
                tmp = Convert.ToInt32(dt.Rows[0]["Trimestre"]);
            }

            return tmp;
        }

        public static int ObtenerUltimoTrimestreJugadoPorBancoIDYPartidaID(int BancoID, int PartidaID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("BancoID", BancoID));
            paramList.Add(new SqlParameter("PartidaID", PartidaID));

            DataTable dt = DAL.DataAccess.GetData("ObtenerUltimoTrimestreJugadoPorBancoIDYPartidaID", paramList);
            int tmp = -1;

            if (dt != null && dt.Rows.Count >= 0)
            {
                tmp = Convert.ToInt32(dt.Rows[0]["Trimestre"]);
            }

            return tmp;
        }

        public static int ObtenerUltimoTrimestreJugadoPorPartidaID(int PartidaID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", PartidaID));

            DataTable dt = DAL.DataAccess.GetData("ObtenerUltimoTrimestreJugadoPorPartidaID", paramList);
            int tmp = -1;

            if (dt != null && dt.Rows.Count >= 0)
            {
                tmp = Convert.ToInt32(dt.Rows[0]["Trimestre"]);
            }

            return tmp;
        }
    }
}
