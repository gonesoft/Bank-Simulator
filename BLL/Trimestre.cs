﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Trimestre
    {
        public decimal TasaCredito { get; set; }
        public decimal TasaPrestamo { get; set; }
        public decimal TasaAhorro { get; set; }
        public decimal TasaCertificado { get; set; }
        public decimal Mercadeo { get; set; }
        public decimal InvestigacionDesarrollo { get; set; }
        public decimal ResponsabilidadSocial { get; set; }
        public bool HaDecidido { get; set; }
        public Indicador Indicador { get; set; }

        public Trimestre(string pTasaLineaCredito, string pTasaAhorro, string pTasaPrestamo, string pTasaCertificado,
            string pMercadeo, string pInvestigacionDesarrollo, string pResponsabilidadSocial)
        {
            TasaCredito = Convert.ToDecimal(pTasaLineaCredito);
            TasaPrestamo = Convert.ToDecimal(pTasaPrestamo);
            TasaAhorro = Convert.ToDecimal(pTasaAhorro);
            TasaCertificado = Convert.ToDecimal(pTasaCertificado);
            Mercadeo = Convert.ToDecimal(pMercadeo);
            InvestigacionDesarrollo = Convert.ToDecimal(pInvestigacionDesarrollo);
            ResponsabilidadSocial = Convert.ToDecimal(pResponsabilidadSocial);
        }

        public Trimestre(decimal pTasaLineaCredito, decimal pTasaAhorro, decimal pTasaPrestamo, decimal pTasaCertificado,
            decimal pMercadeo, decimal pInvestigacionDesarrollo, decimal pResponsabilidadSocial)
        {
            TasaCredito = pTasaLineaCredito;
            TasaAhorro = pTasaAhorro;
            TasaPrestamo = pTasaPrestamo;
            TasaCertificado = pTasaCertificado;
            Mercadeo = pMercadeo;
            InvestigacionDesarrollo = pInvestigacionDesarrollo;
            ResponsabilidadSocial = pResponsabilidadSocial;
        }
    }
}
