﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace BLL
{
    public class Usuario
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Contrasena { get; set; }
        public string ContrasenaEncriptada { get; set; }
        public string Email { get; set; }
        public int UsuarioCreadorID { get; set; }
        public int RoleID { get; set; }
        public DateTime UltimaActualizacion { get; set; }
        public bool Estado { get; set; }

        public static Usuario CrearUsuario(string Nombre, string Contrasena, string Email, int RoleID, int UsuarioCreadorID, bool Estado = true)
        {
            string contrasenaEncriptada = BCrypt.Net.BCrypt.HashPassword(Contrasena);
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("Nombre", Nombre));
            paramList.Add(new SqlParameter("RoleID", RoleID));
            paramList.Add(new SqlParameter("Contrasena", contrasenaEncriptada));
            paramList.Add(new SqlParameter("Email", Email));
            paramList.Add(new SqlParameter("UsuarioCreadorID", UsuarioCreadorID));
            paramList.Add(new SqlParameter("Estado", Estado));
            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("CrearUsuario", paramList);
            }
            catch
            {
                throw;
            }

            return ((dt != null && dt.Rows.Count > 0) ? (Usuario)dt.Rows[0] : null);

        }

        public static IEnumerable<Usuario> ObtenerUsuariosPorNombreParcial(string nombre)
        {
            DataTable dt = DataAccess.GetData("ObtenerUsuariosPorNombreParcial", nombre, "Nombre");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Usuario)dr;
                }
            }
        }

        public static IEnumerable<Usuario> ObtenerUsuarios()
        {
            DataTable dt = DataAccess.GetData("ObtenerUsuarios");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Usuario)dr;
                }
            }
        }

        public static IEnumerable<Usuario> ObtenerUsuariosPorPartidaID(int partidaID)
        {
            DataTable dt = DataAccess.GetData("ObtenerUsuariosPorPartidaID", partidaID, "PartidaID");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Usuario)dr;
                }
            }
        }

        public static IEnumerable<Usuario> ObtenerUsuariosAceptaronPorPartidaID(int partidaID)
        {
            DataTable dt = DataAccess.GetData("ObtenerUsuariosAceptaronPorPartidaID", partidaID, "PartidaID");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Usuario)dr;
                }
            }
        }

        public static Usuario ObtenerUsuarioPorID(int usuarioID)
        {
            DataTable dt = DataAccess.GetData("ObtenerUsuarioPorID", usuarioID);
            return ((dt != null && dt.Rows.Count > 0) ? (Usuario)dt.Rows[0] : null);
        }

        public static Usuario ObtenerUsuarioPorCorreoElectronico(string correoElectronico)
        {
            try
            {
                DataTable dt = DataAccess.GetData("ObtenerUsuarioPorEmail", correoElectronico, "Email");
                return ((dt != null && dt.Rows.Count > 0) ? (Usuario)dt.Rows[0] : null);
            }
            catch
            {
                return null;
            }

        }

        public static IEnumerable<Usuario> ObtenerBancosYUsuariosFaltantePorPartidaYTrimestre(int PartidaID, int Trimestre)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("PartidaID", PartidaID));
            paramList.Add(new SqlParameter("Trimestre", Trimestre));
            DataTable dt = DataAccess.GetData("ObtenerBancosYUsuariosFaltantePorPartidaYTrimestre", paramList);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    yield return (Usuario)dr;
                }
            }

        }

        public static bool UsuarioYaHaDecididoPorPartidaIDYTrimestre(int PartidaID, int Trimestre, int UsuarioID)
        {
            IEnumerable<Usuario> usrs = ObtenerBancosYUsuariosFaltantePorPartidaYTrimestre(PartidaID, Trimestre);
            return usrs.ToList().Find(x => x.ID == UsuarioID) == null;
        }

        public static explicit operator Usuario(DataRow dr)
        {
            Usuario tmp = null;

            if (dr != null)
            {
                tmp = new Usuario();
                tmp.ID = Convert.ToInt32(dr["ID"]);
                tmp.Nombre = dr["Nombre"].ToString();
                tmp.RoleID = Convert.ToInt32(dr["RoleID"]);
                tmp.ContrasenaEncriptada = (dr["Contrasena"] != DBNull.Value ? dr["Contrasena"].ToString() : string.Empty);
                tmp.Email = dr["Email"].ToString();
                tmp.UsuarioCreadorID = Convert.ToInt32(dr["UsuarioCreadorID"]);
                tmp.UltimaActualizacion = Convert.ToDateTime(dr["UltimaActualizacion"]);
                tmp.Estado = (dr["Estado"] != DBNull.Value ? Convert.ToBoolean(dr["Estado"]) : false);
            }

            return tmp;
        }


        public static Usuario Modificar(Usuario usuario)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("ID", usuario.ID));
            paramList.Add(new SqlParameter("Nombre", usuario.Nombre));
            paramList.Add(new SqlParameter("RoleID", usuario.RoleID));
            paramList.Add(new SqlParameter("Contrasena", usuario.Contrasena));
            paramList.Add(new SqlParameter("Estado", usuario.Estado));

            DataTable dt = null;
            try
            {
                dt = DataAccess.ExecuteScalar("ActualizarUsuario", paramList);
            }
            catch
            {
                throw;
            }

            return ((dt != null && dt.Rows.Count > 0) ? (Usuario)dt.Rows[0] : null);
        }

        public static void Eliminar(Usuario usr)
        {
            throw new NotImplementedException();
        }
    }
}
