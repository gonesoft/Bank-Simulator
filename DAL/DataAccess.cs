﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DataAccess
    {
        public static string getConnectionString()
        {
            //TODO: Add this to a configuration file
            return "Data Source=184.168.194.62;Initial Catalog=pegasus;User ID=pegasus;Password=@0160515";
        }

        public static DataTable ExecuteScalar(string procedureName, List<SqlParameter> paramList = null)
        {
            using (var conn = new SqlConnection(getConnectionString()))
            using (var command = new SqlCommand(procedureName, conn)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                if (paramList != null && paramList.Count > 0)
                {
                    foreach (SqlParameter p in paramList)
                    {
                        command.Parameters.Add(p);
                    }
                }

                conn.Open();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());

                return dt;

            }
        }

        public static DataTable GetData(string procedureName, int paramValue, string parameterName = "ID")
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter(parameterName, paramValue));
            return GetData(procedureName, parameters);
        }

        public static DataTable GetData(string procedureName, string paramValue, string parameterName)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter(parameterName, paramValue));
            return GetData(procedureName, parameters);
        }

        public static DataTable GetData(string procedureName, List<SqlParameter> paramList = null)
        {
            DataTable tmp = null;
            using (var conn = new SqlConnection(getConnectionString()))
            using (var command = new SqlCommand(procedureName, conn)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                if (paramList != null && paramList.Count > 0)
                {
                    foreach (SqlParameter p in paramList)
                    {
                        command.Parameters.Add(p);
                    }
                }

                using (SqlDataAdapter da = new SqlDataAdapter(command))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    tmp = ds.Tables[0];
                }
            }
            return (tmp != null ? tmp : null);
        }
    }
}
