﻿<%@ WebHandler Language="C#" Class="Decisiones" %>

using System;
using System.Web;
using BLL;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.SessionState;

public class Decisiones : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.QueryString["action"] != null && context.Request.QueryString["partidaid"] != null)
        {
            if (context.Request.QueryString["action"] == "obtenertodos")
            {
                int partidaId;
                Usuario usuario = (Usuario)context.Session["UsuarioLogueado"];
                Banco banco = Banco.ObtenerPorUsuario(usuario.ID);
                if (usuario != null && banco != null)
                {
                    if (int.TryParse(context.Request.QueryString["partidaid"], out partidaId))
                    {
                        context.Response.ContentType = "application/json";
                        IEnumerable<Decision> decisiones = Decision.ObtenerDecisionesPorPartidaYBanco(partidaId, banco.ID);
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        context.Response.Write(js.Serialize(decisiones));
                    }
                }
            }
        }

        if (context.Request.QueryString["tomardecision"] != null)
        {
            if (context.Request["obj"] != null)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string value = context.Request["obj"];
                string chks = context.Request["chks"];

                List<ValorSeleccionado> valoresSeleccion = (List<ValorSeleccionado>)serializer.Deserialize(chks, typeof(List<ValorSeleccionado>));


                try
                {
                    Usuario usuario = (Usuario)context.Session["UsuarioLogueado"];
                    bool partidaTermino = false;

                    if (usuario != null)
                    {
                        Decision decision = (Decision)serializer.Deserialize(value, typeof(Decision));


                        List<Banco> bancos = Banco.ObtenerBancosConDecisionesPorPartidaID(decision.PartidaID);
                        Banco banco = bancos.Find(b => b.Usuario.ID == usuario.ID);  //Banco.ObtenerPorUsuarioYPartidaID(usuario.ID, decision.PartidaID);
                        decision = Decision.CrearDecision(decision);

                        if (decision != null && banco != null)
                        {

                            banco.Decisiones.Add(decision);
                            if (Decision.HanTomadoDecisiones(decision.PartidaID, decision.Trimestre))
                            {
                                Jugada.asignarClientes(decision.Trimestre, bancos);
                                foreach (Banco b in bancos)
                                {
                                    Jugada.Decidir(b, decision.Trimestre);
                                }

                                //send email to users
                                IEnumerable<Usuario> users = Usuario.ObtenerUsuariosAceptaronPorPartidaID(decision.PartidaID);
                                Partida partida = Partida.ObtenerPorID(decision.PartidaID);
                                int TrimestreActual = Jugada.ObtenerTrimestreActualPorPartidaID(partida.ID);
                                partidaTermino = (TrimestreActual == partida.CantidadTrimestres);

                                if (partidaTermino)
                                {
                                    Partida.FinalizarPartida(partida.ID);                                    
                                }
                                
                                foreach (Usuario u in users)
                                {
                                    if (!partidaTermino)
                                        MailHelper.sendEmail(u.Email, "<html><body><p>Hola, <strong>" + u.Nombre + "</strong></p><p>Contin&uacute;a adelante en el programa&nbsp;<strong>Banquero Joven Popular.</strong></p><p><strong>Ya puedes ingresar nuevamente y continuar tomando tus decisiones.</strong></p><p>Puedes acceder a la partida dando clic a este enlace:</p><p><a href='http://simulator.jadom.org/Jugar.aspx?partidaid=" + partida.ID + "' target='_blank'>http://simulator.jadom.org/Jugar.aspx?partidaid=" + partida.ID + "</a></p><p>Esperamos que aprendas cu&aacute;n importante es el lado humano de las finanzas.</p><p>&iexcl;Gracias por tu tiempo!</p><p><strong>Simulador Banquero Joven Popular</strong></p><p><a href='http://www.simulator.jadom.org/' target='_blank'>www.simulator.jadom.org</a></p></body></html>", "Listo para la próxima ronda - Banquero Joven Popular");
                                    else
                                        MailHelper.sendEmail(u.Email, "<html><body><p>Hola, <strong>" + u.Nombre + "</strong></p><p><strong>Has completado satisfactoriamente el proceso de charlas y las jornadas de estudio de los casos de negocios del programa Banquero Joven Popular</strong>, en cuyo transcurso has apreciado el modelo ético y socialmente responsable que aplica el <strong>Banco Popular Dominicano</strong> para la realización de sus operaciones bancarias, tanto a lo interno como al externo de la organización.</p><p>En este proceso has visto como la<strong> ética y los valores son fundamentales para el buen ejercicio bancario</strong> como garantía de desarrollo sostenible y humano de una nación y sus ciudadanos. Por ello, y conociendo ya la transcendencia del buen manejo de las finanzas, está en nuestro ánimo incentivar a que te conviertas en un agente catalizador de buenos modelos de negocio basados en un liderazgo socialmente responsable. </p><p><strong>Te informamos que la partida ha finalizado y estarás recibiendo más información respecto a todo el proceso por parte de nuestros administradores.</strong></p><p><strong>Atentamente,</strong></p><p>Banquero Joven Popular<br><a href='http://www.simulator.jadom.org'>www.simulator.jadom.org</a></p></body></html>", "Partida Terminó - Banquero Joven Popular");
                                }
                            }


                            //actualizamos las preguntas
                            if (valoresSeleccion != null && valoresSeleccion.Count > 0)
                            {
                                foreach (ValorSeleccionado vs in valoresSeleccion)
                                {
                                    ValorSeleccion.Modificar(vs.ID, vs.Value);
                                }
                            }


                            //hemos tomado esta decision satisfactoriamente
                            //ahora debemos informar al cliente de alguna forma
                            var a = new { status = true, partidaTermino = partidaTermino };

                            value = serializer.Serialize(a);
                            context.Response.Write(value);
                            context.Session["JugadaExitosa"] = true;
                        }
                    }
                }
                catch
                {
                    //este error ocurre probablemente debido a campos vacios
                    var a = new { status = false, partidaTermino = false };
                        context.Session["JugadaExitosa"] = false;
                }

            }
        }

        if (context.Request.QueryString["revisarestado"] != null && context.Request.QueryString["partidaid"] != null)
        {
            int partidaId;

            if (int.TryParse(context.Request.QueryString["partidaid"], out partidaId))
            {
                int TrimestreActual = Jugada.ObtenerTrimestreActualPorPartidaID(partidaId);
                Partida partida = Partida.ObtenerPorID(partidaId);

                int TrimestreAJugar = ((TrimestreActual + 1) <= partida.CantidadTrimestres ? (TrimestreActual + 1) : -1);
                if (TrimestreAJugar > 0)
                {
                    List<Usuario> falta = Usuario.ObtenerBancosYUsuariosFaltantePorPartidaYTrimestre(partidaId, TrimestreAJugar).ToList();
                    List<object> list = new List<object>();

                    foreach (Usuario u in falta)
                    {
                        list.Add(new { ID = u.ID, TrimestreActual = TrimestreAJugar });
                    }

                    context.Response.ContentType = "application/json";
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    context.Response.Write(js.Serialize(list));
                }
                else
                {
                    List<object> list = new List<object>();
                    list.Add(new { TrimestreActual = TrimestreAJugar });
                    context.Response.ContentType = "application/json";
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    context.Response.Write(js.Serialize(list));
                }
            }
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}