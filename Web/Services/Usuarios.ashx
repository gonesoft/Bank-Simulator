﻿<%@ WebHandler Language="C#" Class="Usuarios" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using BLL;

public class Usuarios : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        if (context.Request.QueryString["q"] != null && context.Request.QueryString["action"] != null)
        {
            if (context.Request.QueryString["action"] == "obtenerparcial")
            {
                context.Response.ContentType = "application/json";
                IEnumerable<Usuario> SearchResults = Usuario.ObtenerUsuariosPorNombreParcial(context.Request.QueryString["q"].ToString());
                string content = "[";
                int count = SearchResults.Count();
                int i = 0;

                foreach (Usuario t in SearchResults)
                {
                    if (i < count)
                        content += (i < count && i > 0 ? "," : "") + "{\"text\":\"" + t.Nombre + " - " + t.Email  + "\", \"id\":" + t.ID + " }";
                    i++;
                }
                content += "]";

                context.Response.Write(content);
            }
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}