﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%if ((Master as Main).UsuarioLogueado != null)
      { %>
    <!-- INICIO TABLA PARTIDAS -->
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        Partidas
                    </div>
                </div>
                <div class="widget-body">
                    <div class="mail">
                        <table class="table table-condensed table-striped table-bordered table-hover no-margin">
                            <thead>
                                <tr>
                                    <th style="width: 5%">
                                    </th>
                                    <th style="width: 40%">
                                        Nombre
                                    </th>
                                    <th style="width: 20%" class="hidden-phone">
                                        Fecha de Inicio
                                    </th>
                                    <th style="width: 20%" class="hidden-phone">
                                        Estado Partida
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repPartida" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <span class="name"><a href='<%# getUrl(Eval("Aceptada"), Eval("ID")) %>'>
                                                    <%# Eval("Nombre") %></a></span>
                                            </td>
                                            <td class="hidden-phone">
                                                <%# Eval("FechaInicio") %>
                                            </td>
                                            <td class="hidden-phone">
                                                <%# getEstado(Eval("ID"), Eval("Estado")) %>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FINAL TABLA GANADORES -->
   <% }
      else
      { 
        %>
    
        <%
      }   
    %>
</asp:Content>
