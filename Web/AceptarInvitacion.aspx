﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="AceptarInvitacion.aspx.cs" Inherits="AceptarInvitacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        Invitaci&oacute;n a Participar
                      <span class="mini-title">Elija que desea hacer con esta invitaci&oacute;n
                      </span>
                    </div>
                    <span class="tools">
                        <a class="fs1" aria-hidden="true" data-icon="" data-original-title=""></a>
                    </span>
                </div>
                <div class="widget-body">
                    <div class="stylish-lists">
                      <dl class="no-margin">
                        <dt class="text-error">
                          Nombre De Partida
                        </dt>
                        <dd>
                          <% if (Partida != null) Response.Write(Partida.Nombre); %>
                        </dd>
                        <dt class="text-error">
                          Participantes
                        </dt>
                        <dd>
                          <asp:Literal ID="litUsuarios" runat="server"></asp:Literal>
                        </dd>
                      </dl>
                    </div>
                    <div class="form-horizontal no-margin">
                        <button class="btn btn-info" runat="server" id="btnAceptar" onserverclick="btnAceptar_ServerClick">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                            Aceptar Invitaci&oacute;n
                        </button>
                        <button type="submit" class="btn btn-info" id="btnRechazar" visible="false" runat="server" onserverclick="btnRechazar_ServerClick">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                            Rechazar Invitaci&oacute;n
                        </button>
                    </div>

                </div>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
</asp:Content>

