﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="CrearCuenta.aspx.cs" Inherits="CrearCuenta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            Crear Cuenta
                           
                            <span class="mini-title">Formulario para creaci&oacute;n de Usuarios</span>
                        </div>
                        <span class="tools">
                            <a class="fs1" aria-hidden="true" data-icon="&#xe090;"></a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="horizontal no-margin" id="crearCuenta">
                            <div class="control-group">
                                <label class="control-label" for="name">
                                    Nombre Completo
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox CssClass="span3" placeholder="Nombre Completo" ID="txtNombre" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="bank">
                                    Nombre de Banco
                                </label>
                                <div class="controls controls-row">
                                    <asp:TextBox CssClass="span3" placeholder="Nombre de Banco" ID="txtNombreBanco" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="email1">
                                    Correo Electr&oacute;nico
                               
                                </label>
                                <div class="controls">
                                    <asp:TextBox CssClass="span6" placeholder="Correo Electr&oacute;nico" ID="txtEmail" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="password1">
                                    Rol
                                </label>
                                <div class="controls">
                                    <asp:DropDownList CssClass="span6" runat="server" ID="ddlRole" AppendDataBoundItems="true">
                                        <asp:ListItem Value="-1">Seleccione</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-actions no-margin">
                                <asp:Button CssClass="btn btn-info pull-right" runat="server" ID="btnCrear" Text="Crear Cuenta" OnClick="Btn_CrearUsuario" />
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#<%= btnCrear.ClientID %>").on("click", function () {
                $("#crearCuenta control-group").removeClass("error");
                reset();
                var tmp = $("#<%= txtNombre.ClientID %>");

                if (tmp.val() === "") {
                    mostrarError("Nombre Completo requerido.", tmp, true);
                    return false;
                }
                else {
                    mostrarError("", tmp, false);
                }

                tmp = $("#<%= txtNombreBanco.ClientID %>");

                if (tmp.val() === "") {
                    mostrarError("Nombre de Banco requerido.", tmp, true);
                    return false;
                }
                else {
                    mostrarError("", tmp, false);
                }

                tmp = $("#<%= txtEmail.ClientID %>");

                if (tmp.val() === "") {
                    mostrarError("Correo Electr&oacute;nico requerido.", tmp, true);
                    return false;
                }
                else {
                    mostrarError("", tmp, false);
                }
                
                tmp = $("#<%= ddlRole.ClientID %>");
                
                if (tmp.val() === "-1") {
                    mostrarError("Rol requerido.", tmp, true);
                    return false;
                }
                else {
                    mostrarError("", tmp, false);
                }

            });
        });
    </script>
</asp:Content>

