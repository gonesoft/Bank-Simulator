﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class HTMLHelper
{
    public static void mostrarMensaje(System.Web.UI.Page instance, string mensaje, TipoMensaje tipo)
    {
        string t = (tipo.ToString().ToLower());

        string js = "showError('" + mensaje + "', '" + t + "');";
        instance.ClientScript.RegisterStartupScript(instance.GetType(), "alert", js, true);
    }
}

public enum TipoMensaje
{
    Error,
    Success
}