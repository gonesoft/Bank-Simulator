﻿using System;

public class BaseAdministrationPage : System.Web.UI.Page
{
    private void Page_Init(Object o, EventArgs args)
    {
        if (!IsPostBack)
        {
            if (Session["UsuarioLogueado"] != null)
            {
                BLL.Usuario usr = (BLL.Usuario)Session["UsuarioLogueado"];

                if (usr != null)
                {
                    BLL.Role rol = BLL.Role.ObtenerPorID(usr.RoleID);

                    if (rol != null)
                    {
                        if (rol.Nombre.ToLower() != "administrador")
                        {
                            Response.Redirect("~/NoAutorizado.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/NoAutorizado.aspx");
                    }
                }
                else 
                {
                    Response.Redirect("~/NoAutorizado.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }
    }
}