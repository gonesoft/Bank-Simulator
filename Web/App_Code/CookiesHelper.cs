﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CookiesHelper
{
    public static HttpCookie CreateAuthCookie(string username, string password)
    {
        HttpCookie cookie = new HttpCookie("SimuladorBancario");
        cookie.Value = username + "|" + password;
        cookie.Expires = DateTime.Now.AddYears(1);
        return cookie;
    }

    public static HttpCookie RetrieveAuthCookie(System.Web.UI.Page page)
    {
        HttpCookie cookie = page.Request.Cookies["SimuladorBancario"];
        return cookie;
    }

    public static void DestroyAuthCookie(System.Web.UI.Page page)
    {
        HttpCookie cookie = RetrieveAuthCookie(page);
        cookie.Expires = DateTime.Now.AddDays(-1d);
        page.Response.Cookies.Add(cookie);
    }
}