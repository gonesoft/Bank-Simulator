﻿using BLL;
using System;

public partial class Main : System.Web.UI.MasterPage
{
    public Usuario UsuarioLogueado
    {
        get
        {
            return (Usuario)Session["UsuarioLogueado"];
        }
        set
        {
            Session["UsuarioLogueado"] = value;
        }
    }

    public Banco MiBanco
    {
        get 
        {
            if (Session["MiBanco"] != null)
            {
                return (Banco)Session["MiBanco"];
            }
            else
            {
                Banco b = Banco.ObtenerPorUsuario(UsuarioLogueado.ID);
                Session["MiBanco"] = b;
                return b;
            }
        }
    }

    public Role Rol
    { 
        get
        {
            return (Role)Session["Rol"];
        }
        set
        {
            Session["Rol"] = value;
        }
    }

    public bool EsAdministrador()
    {
        return Rol.Nombre.ToLower() == "administrador";
    }

    public void MakeSubNavVisible(bool value)
    {
        SubNav.Visible = value;
    }

    public void MakeTopNavVisible(bool value)
    {
        TopNav.Visible = value;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        bool isLogged = UsuarioLogueado != null;
        MakeSubNavVisible(isLogged);
        MakeTopNavVisible(isLogged);
        if (Session["PasswordChanged"] != null)
        {
            MostrarMensaje("La contraseña ha sido actualizada satisfactoriamente. Tu sesión ha sido cerrada, y para volver a acceder al sitio es necesario que ingrese con su nueva contraseña.", TipoMensaje.Success, "PasswordChanged");
        }

        if (Session["PartidaExitosa"] != null)
        {
            MostrarMensaje("La partida ha sido creada satisfactoriamente.", TipoMensaje.Success, "PartidaExitosa");
        }

        if (UsuarioLogueado == null)
        {
            if (!(Request.Url.ToString().ToLower().IndexOf("default.aspx") > -1 || Request.Url.ToString().ToLower().IndexOf("ingresar.aspx") > -1
                || Request.Url.ToString().ToLower().IndexOf("olvidocontrasena.aspx") > -1))
            {
                Response.Redirect("~/Ingresar.aspx?urlretorno=" + Request.Url.ToString());
            }
        }
        else
        {
            this.Header.EsAdministrador = EsAdministrador();
            this.TopNav.EsAdministrador = EsAdministrador();
        }
    }

    void MostrarMensaje(string Mensaje, TipoMensaje tipoMensaje, string NombreSession)
    {
        if (Session[NombreSession] != null)
        {
            HTMLHelper.mostrarMensaje(this.Page, Mensaje, tipoMensaje);
            Session.Remove(NombreSession);
        }
    }
}
