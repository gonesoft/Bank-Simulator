﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubNav.ascx.cs" Inherits="WebControls_SubNav" %>
<div class="sub-nav">
    <!-- This is sub navigation -->
    <ul>
        <li>
            <a href="Default.aspx">Mis Partidas
            </a>
        </li>
    </ul>
    <div class="btn-group pull-right">
        <!-- This dropdown menu is for mobile version -->
        <button class="btn btn-primary">
            Opciones
        </button>
        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right">
            <li>
                <a href="Default.aspx">Mis Partidas
                </a>
            </li>
        </ul>
    </div>
</div>
