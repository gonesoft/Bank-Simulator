﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

public partial class WebControls_RightBar : System.Web.UI.UserControl
{
    public Partida Partida { get; set; }
    public int TrimestreActual { get; set; }
    public Banco Banco { get; set; }
    public List<Indicador> Indicadores { get; set; }
    public bool EsUltimoTrimestre { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Partida != null)
            {
                EsUltimoTrimestre = (TrimestreActual == Partida.CantidadTrimestres);
                this.litJuego.Text = Partida.Nombre;
                System.Globalization.CultureInfo info = new System.Globalization.CultureInfo("es-DO");
                System.Threading.Thread.CurrentThread.CurrentCulture = info;
                DateTime date = Partida.FechaInicio;
                this.litFechaInicio.Text = info.TextInfo.ToTitleCase(date.ToLongDateString());

                IEnumerable<Usuario> usuarios = Usuario.ObtenerBancosYUsuariosFaltantePorPartidaYTrimestre(Partida.ID, TrimestreActual + 1);

                this.repJugadores.DataSource = usuarios.ToList();
                this.repJugadores.DataBind();

                this.Indicadores = Indicador.ObtenerIndicadoresPorBancoIDYPartidaID(Banco.ID, Partida.ID).ToList();
            }
        }
    }

    string cantidadFormateada(decimal cantidad)
    {
        string color = "#74b749";
        if (cantidad < 0)
        {
            color = "red";
        }
        else if (cantidad == 0)
        {
            color = "#ffb400";
        }

        return "<span style='color:" + color + "'>" + cantidad.ToMoney() + "</span>"; ;
    }

    public string capacidadCrediticia()
    {
        return cantidadFormateada(Indicadores.OrderByDescending(x => x.Trimestre).FirstOrDefault().CapacidadCrediticia);
    }

    public string capitalTrabajo()
    { 
        BLL.Indicador indicador = Indicadores.OrderByDescending(x => x.Trimestre).FirstOrDefault();

        return cantidadFormateada(indicador.CapitalTrabajo + indicador.CapacidadCrediticia + indicador.ReservasCapital - indicador.CreditosPendientes);
    }

    public string creditosPendientes()
    {
        return cantidadFormateada(Indicadores.OrderByDescending(x => x.Trimestre).FirstOrDefault().CreditosPendientes);
    }

    public string depositosPendientes()
    {
        return cantidadFormateada(Indicadores.OrderByDescending(x => x.Trimestre).FirstOrDefault().DepositosPendientes);
    }

    public string reservasCapital()
    {
        return cantidadFormateada(Indicadores.OrderByDescending(x => x.Trimestre).FirstOrDefault().ReservasCapital);
    }

    public string prestamoOperaciones()
    {
        return cantidadFormateada(Indicadores.OrderByDescending(x => x.Trimestre).FirstOrDefault().PrestamoOperaciones);
    }

    public string depositosOperaciones()
    {
        return cantidadFormateada(Indicadores.OrderByDescending(x => x.Trimestre).FirstOrDefault().DepositosOperaciones);
    }
}