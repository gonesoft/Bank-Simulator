﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

public partial class WebControls_TomaDecisiones : System.Web.UI.UserControl
{
    public int TrimestreActual;
    public int UltimoTrimestreJugado;
    public Partida Partida;
    public Banco Banco;
    public bool HaDecidido = true;
    public int TrimestreAJugar;
    public List<Caso> Casos = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        TrimestreAJugar = ((TrimestreActual + 1) <= Partida.CantidadTrimestres ? (TrimestreActual + 1) : -1);
        Casos = Caso.ObtenerTodos(TrimestreActual, Banco.ID, Partida.ID);
        if (TrimestreAJugar > 0)
        {
            HaDecidido = Usuario.UsuarioYaHaDecididoPorPartidaIDYTrimestre(Partida.ID, TrimestreAJugar, Banco.Usuario.ID);
        }
    }

    public string isActive(int Trimestre)
    {
        string tmp = "";

        if (TrimestreAJugar == -1)
        {
            if (Trimestre >= Partida.CantidadTrimestres)
            {
                tmp = " active";
            }
        }
        else
        {
            if (Trimestre == TrimestreAJugar)
            {
                tmp = " active";
            }
        }
        return tmp;
    }

    public string getChecked(Enunciado e)
    {
        string str = "";
        if (e.Seleccionado != null && e.Seleccionado == true)
        {
            str = "checked";
        }

        //if (TrimestreActual >= e.ValorID)
        //    str += " disabled='disabled'";

        return str;
    }

    public string getCorrect(Enunciado e)
    {
        string str = "";

        if (e.EsCorrecta != null)
        {
            if ((bool)e.Seleccionado)
            {
                str = "class='";
                str += (bool)e.EsCorrecta ? "correct" : "incorrect";
                str += "'";
            }
        }

        return str;
    }

    public string getDisabled(int i)
    {
        if (HaDecidido)
        {
            return "disabled='disabled'";
        }
        return "";
    }

    public List<ValorEnunciados> getAllScrambled(int trimestre)
    {
        List<ValorEnunciados> todosValores = null;

        IEnumerable<Valor> todos = Valor.ObtenerTodos();

        if (todos != null && todos.Count() > 0)
        {
            todosValores = new List<ValorEnunciados>();

            foreach (Valor v in todos)
            {
                List<Enunciado> scrambled = getScrambled(v, trimestre);
                todosValores.Add(new ValorEnunciados() { Enunciados = scrambled, Valor = v });
            }
        }

        return todosValores;
    }

    public List<Enunciado> getScrambled(Valor v, int trimestre)
    {
        List<Enunciado> scrambled = Enunciado.ObtenerEnunciadosValoresSeleccionPorPartidaIDYUsuarioIDYValorIDYTrimestre(Partida.ID, Banco.Usuario.ID, v.ID, trimestre).ToList();

        //if the list has been already saved, get it from the DB, otherwise create a new list, and save it

        if (scrambled == null || scrambled.Count <= 0)
        {
            scrambled = new List<Enunciado>();

            scrambled.AddRange(BLL.ListHelper.GetRandomElements(v.EnunciadosCorrectos, 3));
            scrambled.AddRange(BLL.ListHelper.GetRandomElements(v.EnunciadosIncorrectos, 2));
            scrambled.Shuffle();

            foreach (Enunciado e in scrambled)
            {
               e.ValorSeleccionID = ValorSeleccion.CrearValorSeleccion(Partida.ID, Banco.Usuario.ID, e.ID, e.ValorID, trimestre);
            }
        }
        return scrambled;
    }


}