﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopNav.ascx.cs" Inherits="WebControls_TopNav" %>
<div class="top-nav">
    <!-- This is main navigation -->
    <ul>
    <%
        if (EsAdministrador)
        {
     %>
    
        <li>
            <a href="Default.aspx" class="selected">
                <div class="fs1" aria-hidden="true" data-icon="&#xe0a0;"></div>
                Admin
            </a>
        </li>
    <%    
        } 
        else
        {
    %>
        <li>
            <a href="Default.aspx" class="selected">
                <div class="fs1" aria-hidden="true" data-icon=""></div>
                Partidas
            </a>
        </li>
    <%
        }
     %>
    </ul>
    <div class="clearfix">
    </div>
</div>
