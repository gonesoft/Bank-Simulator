﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TomaDecisiones.ascx.cs" Inherits="WebControls_TomaDecisiones" %>
<div class="widget-body">
    <ul id="mainTabs" class="nav nav-tabs no-margin myTabBeauty">
        <%
            if (Partida != null)
            {
                for (int i = 1; i < Partida.CantidadTrimestres + 1; i++)
                { 
        %>
        <li class='<%= isActive(i) %> <%= (i == 0 || i > TrimestreActual + 1 ? "disabled" : "") %>'>
            <a data-toggle="tab" href="<%=((i > TrimestreActual + 1) ? "": "#trimestre_" +  i ) %>" data-original-title=""><span class="fs1" aria-hidden="true" data-icon=""></span>&nbsp;<%= "Ronda " + i %>
            </a>
        </li>
        <%
                }
            }
        %>
    </ul>
    <div class="tab-content">
        <%
            if (Partida != null)
            {
                for (int i = 0; i < Partida.CantidadTrimestres + 1; i++)
                { 
        %>
        <div id="trimestre_<%= i %>" class="tab-pane fade in<%= isActive(i) %>">
            <% if (i > 0)
               { %>
            <div>
                <div class="alert alert-dismissable alert-info">
                    <!--<button type="button" class="close" data-dismiss="alert">×</button>-->
                    <strong><%= Casos[i - 1].Titulo %>:</strong><br />
                    <%= Casos[i - 1].Text %>
                </div>
            </div>
            <% } %>
            <h5>Decisiones de la Ronda <%= i %></h5>
            <hr />
            <div>
                <div class="no-margin span12 row-fluid form-horizontal" role="form">
                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label">
                                Tasa Tarjeta de Cr&eacute;dito (%)
                            </label>
                            <div class="controls">
                                <input type="text" class="percentage" name="credito_<%= i %>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Tasa Pr&eacute;stamo Hipotecario o Veh&iacute;culo(%)
                            </label>
                            <div class="controls">
                                <input type="text" class="percentage" name="prestamo_<%= i %>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Tasa Cuentas de Ahorro y Corriente (%)
                            </label>
                            <div class="controls">
                                <input type="text" class="percentage" name="ahorro_<%= i %>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Tasa de Certificado Financiero e Inversiones (%)
                            </label>
                            <div class="controls">
                                <input type="text" class="percentage" name="certificado_<%= i %>">
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label" for="email1">
                                Inversi&oacute;n en Mercadeo
                            </label>
                            <div class="controls">
                                <input type="text" class="money" name="mercadeo_<%= i %>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Inversi&oacute;n en Tecnolog&iacute;a y Calidad de Servicios
                            </label>
                            <div class="controls">
                                <input type="text" class="money" name="iyd_<%= i %>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Inversi&oacute;n en Responsabilidad Social y Capital Humano
                            </label>
                            <div class="controls">
                                <input type="text" class="money" name="respsocial_<%= i %>">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                    <% 
                    if (i > 0)
                    {
                        List<BLL.ValorEnunciados> ve = getAllScrambled(i);
                    %>
                    <ul class="nav nav-tabs no-margin myTabBeauty">
                        <%
                        if (ve != null)
                        {
                            bool isFirst = true;
                            foreach (BLL.ValorEnunciados vee in ve)
                            {
                        %>

                        <li class="<%= (isFirst? "active":"") %>">
                            <a data-toggle="tab" href="#valores_<%= i %>_<%= vee.Valor.ID %>" data-original-title=""><span class="fs1" aria-hidden="true" data-icon=""></span>&nbsp;<%= vee.Valor.Nombre %></a>
                        </li>
                        <%
                                if (isFirst) isFirst = false;
                            }
                            
                        %>
                    </ul>
                    <div class="tab-content">
                        <%
                            isFirst = true;
                             foreach (BLL.ValorEnunciados vee in ve)
                            {
                             %>
                                <div id="valores_<%= i %>_<%= vee.Valor.ID %>" class="tab-pane fade in <%= (isFirst? "active":"") %> ">
                                    <div>
                                        <%
                                            Response.Write("<div class='chkgroup' data-trimestre='" + i + "' style='padding-left: 40px'><h2 class='text-info'>" + vee.Valor.Nombre + "</h2><h4><strong><i>" + @"""" + vee.Valor.MiValor + @"""" + "</i></strong></h4><hr />");
                                            
                                            List<BLL.Enunciado> scrambled = vee.Enunciados;
                                            foreach (BLL.Enunciado e in scrambled)
                                            {
                                                Response.Write("<div class='checkbox'><input " + getDisabled(i) + " id='chk_trimestre" + i + "_" + e.ID + "' data-valoresseleccionid='" + e.ValorSeleccionID + "' type='checkbox' " + getChecked(e) + " /><label for='chk_trimestre" + i + "_" + e.ID + "' " + getCorrect(e) + ">" + e.MiEnunciado + "</label></div>");
                                            }
                                            Response.Write("</div><br />");
                                            if (isFirst) isFirst = false;
                                        %>
                                    </div>
                                </div>
                        <% 
                        }
                           
                            
                            %>
                    </div>
                    <%
                        }
                    }
                    //esconde el boton 
                    if (i == (UltimoTrimestreJugado + 1) && !HaDecidido)
                    { 
                    %>
                    <div class="form-actions no-margin" id="panelDecidir_<%= i %>">
                        <button id="btnJugar" type="button" class="btn btn-info pull-right" data-trimestre_id="<%= i %>">
                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                            Tomar Decisi&oacute;n
                        </button>
                        <div class="clearfix">
                        </div>
                    </div>
                    <% } %>
                </div>

            </div>
        </div>
        <%
                }
            }
        %>
    </div>
</div>