﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BarraJuego.ascx.cs" Inherits="WebControls_RightBar" %>
<div class="right-sidebar">
    <div class="wrapper" style="overflow-y:scroll; height: 600px;">
        <div class="mini-dashboard">
            <div class="graph-container">
                <p class="city">
                    <asp:Literal ID="litJuego" runat="server"></asp:Literal>
                    <br />
                    <span class="time">
                        <asp:Literal ID="litFechaInicio" runat="server"></asp:Literal>
                    </span>
                </p>
            </div>
            <ul class="month-income" id="ulJugadores" data-trimestreactual="<%= TrimestreActual %>" data-ultimotrimestre="<%= EsUltimoTrimestre %>">
                <asp:Repeater ID="repJugadores" runat="server">
                    <ItemTemplate>
                        <li data-usuarioid='<%# Eval("ID") %>' data-nombre='<%# Eval("Nombre") %>'>
                            <span class="icon-block blue-block">
                                <img src="../img/loguitopopular.png" />
                            </span>
                            <h5><%# Eval("Nombre") %>
                            </h5>
                            <p>
                                <% if (TrimestreActual == Partida.CantidadTrimestres)
                                   {
                                       Response.Write("Finalizó la Partida.");
                                   }
                                   else
                                   {
                                       Response.Write("Falta por jugar &eacute;sta Ronda.");
                                   }
                                %>
                            </p>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
    <div class="wrapper">
        <ul class="stats">
            <li>
                <div class="left">
                    <h4><%= capitalTrabajo() %>
                    </h4>
                    <p>
                        Capital de Trabajo
                    </p>
                </div>
                <div class="chart">
                    <span id="capital-trabajo">
                        <%
                            foreach (BLL.Indicador indicador in Indicadores)
                            {
                                Response.Write(indicador.CapitalTrabajo + indicador.CapacidadCrediticia + indicador.ReservasCapital - indicador.CreditosPendientes + ((indicador == Indicadores.Last()) ? "" : ","));
                            }    
                        %>
                    </span>
                </div>
            </li>
        </ul>
        <ul class="stats">
            <li>
                <div class="left">
                    <h4><%= capacidadCrediticia() %>
                    </h4>
                    <p>
                        Capacidad Crediticia
                    </p>
                </div>
                <div class="chart">
                    <span id="capacidad-crediticia">
                        <%
                            foreach (BLL.Indicador indicador in Indicadores)
                            {
                                Response.Write(indicador.CapacidadCrediticia + ((indicador == Indicadores.Last()) ? "" : ","));
                            }    
                        %>
                    </span>
                </div>
            </li>
        </ul>
        <ul class="stats">
            <li>
                <div class="left">
                    <h4><%= creditosPendientes() %>
                    </h4>
                    <p>
                        Cr&eacute;ditos Pendientes
                    </p>
                </div>
                <div class="chart">
                    <span id="creditos-pendientes">
                        <%
                            foreach (BLL.Indicador indicador in Indicadores)
                            {
                                Response.Write(indicador.CreditosPendientes + ((indicador == Indicadores.Last()) ? "" : ","));
                            }    
                        %>
                    </span>
                </div>
            </li>
        </ul>
        <ul class="stats">
            <li>
                <div class="left">
                    <h4><%= reservasCapital() %>
                    </h4>
                    <p>
                        Reservas de Capital
                    </p>
                </div>
                <div class="chart">
                    <span id="reservas-capital">
                        <%
                            foreach (BLL.Indicador indicador in Indicadores)
                            {
                                Response.Write(indicador.ReservasCapital + ((indicador == Indicadores.Last()) ? "" : ","));
                            }    
                        %>
                    </span>
                </div>
            </li>
        </ul>
    </div>
</div>
