﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="WebControls_Header" %>
<header>
    <a href='<%= ResolveUrl("~/Default.aspx") %>' class="logo">
        <img src="img/logo.png" alt="Logo" /><img src="img/franjalogos.png" />
    </a>

    <%
        if (Session["UsuarioLogueado"] != null)
        {  %>
    <div class="btn-group">
        <button class="btn btn-primary">

            <%= ((BLL.Usuario)Session["UsuarioLogueado"]).Nombre %>
        </button>
        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">
            <span class="caret"></span>
        </button>
        <% 
            if (EsAdministrador)
            {   
        %>
        <ul class="dropdown-menu pull-right">
            <li>
                <a href='<%= ResolveUrl("~/CrearCuenta.aspx") %>'>Nueva Cuenta
                </a>
            </li>
            <li>
                <a href='<%= ResolveUrl("~/NuevaPartida.aspx") %>'>Nueva Partida
                </a>
            </li>
            <li>
                <a href='<%= ResolveUrl("~/CambiarContrasena.aspx") %>'>Cambiar Contraseña
                </a>
            </li>
            <li>
                <a href='<%= ResolveUrl("~/Salir.aspx") %>?urlretorno=<%= this.Request.Url %>'>Salir
                </a>
            </li>
        </ul>
        <%}
            else
            { %>
        <ul class="dropdown-menu pull-right">
            <li>
                <a href='<%= ResolveUrl("~/CambiarContrasena.aspx") %>'>Cambiar Contraseña
                </a>
            </li>
            <li>
                <a href='<%= ResolveUrl("~/Salir.aspx") %>?urlretorno=<%= this.Request.Url %>'>Salir
                </a>
            </li>
        </ul>
        <% } %>
    </div>
    <!--<ul class="mini-nav">
        <li>
            <a href="#">
                <div class="fs1" style="color: black" aria-hidden="true" data-icon="&#xe040;"></div>
                <span class="info-label badge badge-warning">1
                </span>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="fs1" style="color: black" aria-hidden="true" data-icon="&#xe04c;"></div>
                <span class="info-label badge badge-info">2
                </span>
            </a>
        </li>
    </ul>-->
    <% }
        else
        { %>
    <div class="btn-group">
        <% if (Request.Url.ToString().IndexOf("Ingresar.aspx") > -1)
           { %>
        <a class="btn btn-primary" href='<%= ResolveUrl("~/Ingresar.aspx") %>'><span class="fs1" aria-hidden="true" data-icon=""></span>Ingresar</a>
        <% }
           else
           { %>
        <a class="btn btn-primary" href='<%= ResolveUrl("~/Ingresar.aspx") %>?urlretorno=<%= Request.Url %>'><span class="fs1" aria-hidden="true" data-icon=""></span>Ingresar</a>
        <% } %>
    </div>
    <%
                
        }
    %>
</header>
