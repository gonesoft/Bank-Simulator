﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

public partial class AceptarInvitacion : System.Web.UI.Page
{
    public Partida Partida;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["partidaid"] != null)
        {
            int partidaId;

            if (int.TryParse(Request.QueryString["partidaid"], out partidaId))
            {
                this.Partida = Partida.ObtenerPorID(partidaId);

                if (this.Partida != null)
                {
                    //si es un administrador, llevarlo a la pagina donde puede
                    //ver las partidas
                    if ((Master as Main).EsAdministrador())
                    {
                        Response.Redirect("~/Administrador/VerPartidas.aspx?partidaid=" + partidaId);
                    }

                    //verificar si estoy invitado a esta partida
                    UsuarioPartida miInvitacion = UsuarioPartida.ObtenerUsuarioPartidaPorPartidaIDYUsuarioID(partidaId, (Master as Main).UsuarioLogueado.ID);

                    if (miInvitacion != null)
                    {
                        //ya que es mi invitacion, sacar si la partida ya ha comenzado
                        bool hanDecidido = UsuarioPartida.HanTomadoAcciones(this.Partida.ID);

                        if (hanDecidido)
                        {
                            partidaInvalida("HanIniciado");
                        }

                        //mostrar otros usuarios a esta partida
                        IEnumerable<Usuario> invitados = Usuario.ObtenerUsuariosPorPartidaID(partidaId);
                        if (invitados != null && invitados.Count() > 0)
                        {
                            this.litUsuarios.Text = "<div class='stylish-lists'><ul>\n";
                            foreach (Usuario up in invitados)
                            {
                                this.litUsuarios.Text += "<li>" + up.Nombre + "</li>\n";
                            }
                            this.litUsuarios.Text += "</ul></div>\n";
                        }
                    }
                    else
                    {
                        //no invitado aca
                        partidaInvalida("NoInvitado");
                    }
                }
                else
                {
                    partidaInvalida();
                }
            }
            else
            {
                partidaInvalida();
            }
        }
        else
        {
            partidaInvalida();
        }
    }

    void partidaInvalida(string nombreSesion = "PartidaInvalida")
    {
        Session[nombreSesion] = true;
        Response.Redirect("~/Default.aspx");
    }

    void TomarAccion(bool accion)
    {
        UsuarioPartida partida = UsuarioPartida.ObtenerUsuarioPartidaPorPartidaIDYUsuarioID(this.Partida.ID, (Master as Main).UsuarioLogueado.ID);

        if (partida != null)
        {
            partida.Aceptada = accion;

            partida = UsuarioPartida.Modificar(partida);

            if (partida != null)
            {
                //todo salio bien
                if (accion)
                {
                    Session["PartidaAceptada"] = true;
                }
                else
                {
                    Session["PartidaRechazada"] = true;
                }


                //verificamos el estado de las invitaciones, y enviamos correos
                //si todos han aceptado/rechazado

                bool hanDecidido = UsuarioPartida.HanTomadoAcciones(this.Partida.ID);

                if (hanDecidido)
                {
                    //enviar correos
                    IEnumerable<Usuario> aceptaron = Usuario.ObtenerUsuariosAceptaronPorPartidaID(this.Partida.ID);

                    if (aceptaron != null && aceptaron.Count() > 0)
                    {
                        IniciarPartida();

                        //TODO: un jugador no debe ser permitido jugar si es el unico que acepto
                        string listaUsuarios = "<ul>";
                        foreach (Usuario usr in aceptaron)
                        {
                            listaUsuarios += "<li>" + usr.Nombre + "</li>";
                        }

                        listaUsuarios += "</ul>";

                        if (!HTTPHelper.IsLocal())
                        {
                            List<string> emails = new List<string>();
                            foreach (Usuario usr in aceptaron)
                            {
                                emails.Add(usr.Email);
                            }

                            MailHelper.sendEmail("equiposimulador@jadom.org", "<html><head><title></title></head><body><p>Hola, &nbsp;<br /><br />Contin&uacute;a adelante en el programa <strong>Banquero Joven Popular</strong> y aprender&aacute;s c&oacute;mo llegar a ser un profesional &eacute;tico y con &eacute;xito en los negocios.</p><p>La partida virtual <strong>" + this.Partida.Nombre + "</strong> de nuestro Simulador Bancario ha comenzado. <p>A continuaci&oacute;n, te presentamos los otros jugadores que han aceptado la invitaci&oacute;n:</p>" + listaUsuarios + "<p><br />Puedes acceder a la partida dando clic a este enlace:<br /><br /><a href='http://simulator.jadom.org/Jugar.aspx?partidaid=" + Partida.ID + "'>http://simulator.jadom.org/Jugar.aspx?partidaid=" + Partida.ID + "</a><br /><br />Esperamos que aprendas cu&aacute;n importante es el lado humano de las finanzas.</p><p>&iexcl;Gracias por tu tiempo!<br /><br /><strong>Simulador Banquero Joven Popular</strong><br /><a href='http://www.simulator.jadom.org'>www.simulator.jadom.org</a></p></body></html>", "La partida ha comenzado - Banquero Joven Popular", emails);
                            //MailHelper.sendEmail(usr.Email, "Distinguido <strong>" + usr.Nombre + "</strong>, <br /><br />la partida virtual del Simulador Bancario nombrada <strong>" + this.Partida.Nombre + "</strong> ha comenzado. A continuación los otros jugadores que han aceptado la invitación: <br /><br />" + listaUsuarios + "<br /><br />Puedes acceder a la partida dando clic en este enlace: <br /><br /><a href='http://simulator.jadom.org/Jugar.aspx?partidaid=" + Partida.ID + "' target='_blank'>http://simulator.jadom.org/Jugar.aspx?partidaid=" + Partida.ID + "</a><br /><br /> Gracias por su tiempo, <br /><br /> <strong>Simulador Bancario</strong>", "La partida ha comenzado - Simulador Bancario");
                        }
                    }
                }


                Response.Redirect("~/Default.aspx");
            }
        }
        else
        {
            partidaInvalida();
        }
    }

    private void IniciarPartida()
    {
        //cargamos los bancos
        IEnumerable<Banco> bancos = Banco.ObtenerBancosPorPartidaID(this.Partida.ID);
        //iniciamos la partida

        if (bancos != null && bancos.Count() > 0)
        {
            Jugada jugada = new Jugada(bancos.ToList(), this.Partida.CantidadTrimestres, this.Partida.ID);

            if (jugada != null)
            {
                jugada.Comenzar();
            }
        }

    }

    protected void btnAceptar_ServerClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            TomarAccion(true);
        }
    }

    protected void btnRechazar_ServerClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            TomarAccion(false);
        }
    }
}