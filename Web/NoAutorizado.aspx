﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="NoAutorizado.aspx.cs" Inherits="NoAutorizado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid page-not-found">
        <div class="span6 number">
            400
        </div>
        <div class="span6">
            <h3>No Autorizado
            </h3>
            <p>
                Usted no tiene suficientes privilegios para ver esta página.
                  <br />
                Si piensa que esto es un error, favor contactar al administrador del sitio.
            </p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
</asp:Content>

