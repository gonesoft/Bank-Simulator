﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

public partial class NuevaPartida : BaseAdministrationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void btnCrearPartida_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hiddenUsuarios.Value))
        {
            string concatenados = hiddenUsuarios.Value;
            string[] sep = new string[] { "," };
            string[] userString = concatenados.Split(sep, StringSplitOptions.RemoveEmptyEntries);

            if (userString != null && userString.Count() > 0)
            {
                //sabemos que tenemos usuarios para jugar, procedemos a guardar todo\

                //guardamos la partida

                Partida partida = new Partida();
                partida.Nombre = this.txtNombrePartida.Text;
                partida.AdministradorID = (Master as Main).UsuarioLogueado.ID;
                partida.CantidadTrimestres = Convert.ToInt16(this.ddlTrimestres.SelectedItem.Value);
                partida.Estado = true;

                partida = Partida.CrearPartida(partida);

                if (partida != null && partida.ID > 0)
                {
                    UsuarioPartida up = null;
                    Usuario user = null;

                    foreach (string st in userString)
                    {
                        user = Usuario.ObtenerUsuarioPorID(Convert.ToInt32(st));

                        if (user != null)
                        {
                            up = UsuarioPartida.CrearUsuarioPartida(new UsuarioPartida(user.ID, partida.ID, null));
                            if (up != null)
                            {
                                //enviar correo
                                MailHelper.sendEmail(user.Email, "<html><head><title></title></head><body><p>Hola, <strong>" + user.Nombre + "</strong><br /><br />Bienvenido al programa <strong>Banquero Joven Popular</strong>. Queremos que aprendas a conocer el lado humano de las finanzas.</p><p>Te invitamos a tomar parte de <strong>una partida de cinco rondas en nuestro Simulador Bancario</strong>. <br />Para aceptar esta invitaci&oacute;n, haz clic en el siguiente enlace:</p><p><strong><a href='http://simulator.jadom.org/AceptarInvitacion.aspx?partidaid=" + partida.ID + "' target='_blank'>Clic Aqu&iacute; para aceptar</a></strong> &nbsp;<br /><br />Convi&eacute;rtete en un profesional &eacute;tico, respetuoso de los dem&aacute;s y comprometido con tu comunidad, y lograr&aacute;s el &eacute;xito en tus emprendimientos.</p><p>&iexcl;Gracias por tu tiempo!</p><p><br /><strong>Simulador Banquero Joven Popular</strong><br /><a href='http://www.simulator.jadom.org'>www.simulator.jadom.org</a></p><p>&nbsp;</p></body></html>", "Invitación a la Partida - Banquero Joven Popular");
                                //MailHelper.sendEmail(user.Email, "Distinguido <b>" + user.Nombre + "</b>,<br /><br /> Usted ha sido invitado a tomar parte de una partida de " + partida.CantidadTrimestres + " rondas en nuestro Simulador Bancario. Si desea aceptar o rechazar esta invitación, favor de dar clic al siguiente enlace: <a href='http://simulator.jadom.org/AceptarInvitacion.aspx?partidaid=" + partida.ID + "' target='_blank'>http://simulator.jadom.org/AceptarInvitacion.aspx?partidaid=" + partida.ID + "</a>. <br /><br /> Gracias por su tiempo! <br /><br /> El equipo de <b><i>Simulador Bancario.org</b></i>", "Simulador Bancario - Invitación a Partida");
                            }
                        }
                    }
                    Session["PartidaExitosa"] = true;
                    Response.Redirect(Request.Url.ToString());
                }
            }
        }
    }
}