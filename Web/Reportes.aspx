﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Reportes.aspx.cs" Inherits="Reportes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- INICIO TABLA GANADORES -->
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        Jugadores 
                    </div>
                </div>
                <div class="widget-body">
                    <div class="mail">
                        <table class="table table-condensed table-striped table-bordered table-hover no-margin">
                            <thead>
                                <tr>
                                    <th style="width: 5%">
                                        <!--<input type="checkbox" class="no-margin" />-->
                                    </th>
                                    <th style="width: 40%">Nombre
                                    </th>
                                    <th style="width: 20%" class="hidden-phone">Posición
                                    </th>
                                    <th style="width: 10%" class="hidden-phone">
                                        <div align="center">
                                            Status
                                        </div>
                                    </th>

                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repResultado" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td></td>
                                            <td><span class="name"><%# Eval("Nombre") %></span></td>
                                            <td class="hidden-phone"><%# Eval("Lugar") %></td>
                                            <td class="hidden-phone">
                                                <span class="label label label-info">Jugador
                                                </span>
                                            </td>
                                            <td class="hidden-phone"><%# Eval("Puntos") %> ( Decisiones: <%# Eval("PuntosDecisiones") %>, Valores: <%# Eval("PuntosValores") %>)
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FINAL TABLA GANADORES -->

    <!-- INICIO TABLA TASAS -->
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        Decisiones de Tasas<a id="dynamicTable" data-original-title=""></a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">

                            <thead>
                                <tr>
                                    <th style="width: 12%">Ronda</th>
                                    <th style="width: 20%">Tasa Tarjeta de Crédito</th>
                                    <th style="width: 24%">Tasa Préstamo Hipotecario o Vehículo (%)</th>
                                    <th style="width: 20%" class="hidden-phone">Tasa Cuentas de Ahorro y Corriente (%)</th>
                                    <th style="width: 24%" class="hidden-phone">Tasa de Certificado Financiero e Inversiones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repTasas" runat="server">
                                    <ItemTemplate>
                                        <tr class="gradeX">
                                            <td><%# BLL.NumberHelper.AOrdinal(Convert.ToInt32(Eval("Trimestre")), true) %> Ronda</td>
                                            <td><%# Eval("TasaCredito") %>%</td>
                                            <td><%# Eval("TasaPrestamo") %>%</td>
                                            <td><%# Eval("TasaAhorro") %>%</td>
                                            <td><%# Eval("TasaCertificado") %>%</td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FINAL TABLA TASAS -->

    <!-- INICIO TABLA INVERSION -->
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        Decisiones de Inversiones<a data-original-title=""></a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left">

                            <thead>
                                <tr>
                                    <th style="width: 12%">Ronda</th>
                                    <th style="width: 20%">Mercadeo</th>
                                    <th style="width: 24%">Tecnolog&iacute;a y Calidad de Servicios</th>
                                    <th style="width: 20%" class="hidden-phone">Responsabilidad Social y Capital Humano</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repInversion" runat="server">
                                    <ItemTemplate>
                                        <tr class="gradeX">
                                            <td><%# BLL.NumberHelper.AOrdinal(Convert.ToInt32(Eval("Trimestre")), true) %> Ronda</td>
                                            <td><%#  BLL.MoneyHelper.ToMoney(Convert.ToDecimal(Eval("Mercadeo"))) %></td>
                                            <td><%# BLL.MoneyHelper.ToMoney(Convert.ToDecimal(Eval("InvestigacionDesarrollo"))) %></td>
                                            <td><%# BLL.MoneyHelper.ToMoney(Convert.ToDecimal(Eval("ResponsabilidadSocial"))) %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FINAL TABLA TASAS -->

    <!-- INICIO TABLA INVERSION -->
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        Movimientos de Capital<a data-original-title=""></a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left">

                            <thead>
                                <tr>
                                    <th style="width: 33%">Ronda</th>
                                    <th style="width: 33%">Capital de Trabajo</th>
                                    <th style="width: 34%">Capacidad Cr&eacute;diticia</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repMovimientos" runat="server">
                                    <ItemTemplate>
                                        <tr class="gradeX">
                                            <td><%# BLL.NumberHelper.AOrdinal(Convert.ToInt32(Eval("Trimestre")), true) %> Ronda</td>
                                            <td><%#  getCapital((BLL.Indicador)Container.DataItem) %></td>
                                            <td><%# BLL.MoneyHelper.ToMoney(Convert.ToDecimal(Eval("CapacidadCrediticia"))) %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FINAL TABLA TASAS -->

        <!-- INICIO TABLA ENUNCIADOS -->
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        Enunciados Correctos<a data-original-title=""></a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="example_alt_pagination">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left">

                            <thead>
                                <tr>
                                    <th style="width: 100%">Enunciado</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repValores" runat="server">
                                    <ItemTemplate>
                                        <tr class="gradeX">
                                            <td><%# Eval("MiEnunciado") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FINAL TABLA TASAS -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightBar" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="footer" runat="Server">
</asp:Content>

