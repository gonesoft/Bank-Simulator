(function ($) {
    $.fn.money_field = function (options) {
        var defaults = {
            width: null
        };
        var options = $.extend(defaults, options);

        return this.each(function () {
            obj = $(this);
            if (options.width)
                obj.css('width', options.width + 'px');
            obj.wrap("<div class='input-prepend'>");
            obj.before("<span class='add-on'>$</span>");
            obj.on("blur", function () {
                var value = Number($(this).val().replace(",", ""));
                $(this).val(value.format(2));
            });

            obj.keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and . and refresh
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 116]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            obj.attr("maxlength", "20")
        });
    };
})(jQuery);