﻿function getDecision(partidaID, bancoID, trimestre, tasaCredito, tasaPrestamo, tasaAhorro, tasaCertificado, mercadeo, investigacionDesarrollo, responsabilidadSocial) {
    var decision = new Object();
    decision.PartidaID = partidaID;
    decision.BancoID = bancoID;
    decision.Trimestre = trimestre;
    decision.TasaCredito = parseFloat(tasaCredito);
    decision.TasaPrestamo = parseFloat(tasaPrestamo);
    decision.TasaAhorro = parseFloat(tasaAhorro);
    decision.TasaCertificado = parseFloat(tasaCertificado);
    decision.Mercadeo = parseFloat(mercadeo.replace(/,/g, ''));
    decision.InvestigacionDesarrollo = parseFloat(investigacionDesarrollo.replace(/,/g, ''));
    decision.ResponsabilidadSocial = parseFloat(responsabilidadSocial.replace(/,/g, ''));

    return decision;
}

function getCheck(chk) {
    var $chk = $(chk);
    var c = new Object();
    c.ID = $chk.attr("data-valoresseleccionid");
    c.Value = $chk.prop("checked");

    return c;
}

$(function () {
    //binds dynamically generated elements, since we don't know how many
    //buttons and tabs (trimestres) we will have, so we bind it to the tabs element
    //(which will always be there)
    $(".tab-content").on("click", "button[data-trimestre_id]", function () {
        var url = servicesUrl + "/Decisiones.ashx?tomardecision=true";
        var trimestreId = $(this).data("trimestre_id");

        //don't accept trimestre 0
        if (trimestreId > 0) {
            //we validate the decision
            if (validateDecision(trimestreId)) {
                if (validateValores(trimestreId)) {

                    var tasaCredito = $("input[name='credito_" + trimestreId + "']").val();
                    var tasaPrestamo = $("input[name='prestamo_" + trimestreId + "']").val();
                    var tasaAhorro = $("input[name='ahorro_" + trimestreId + "']").val();
                    var tasaCertificado = $("input[name='certificado_" + trimestreId + "']").val();
                    var mercadeo = $("input[name='mercadeo_" + trimestreId + "']").val();
                    var investigacionDesarrollo = $("input[name='iyd_" + trimestreId + "']").val();
                    var responsabilidadSocial = $("input[name='respsocial_" + trimestreId + "']").val();

                    var decision = getDecision(partidaID, miBancoID, trimestreId, tasaCredito, tasaPrestamo, tasaAhorro, tasaCertificado, mercadeo, investigacionDesarrollo, responsabilidadSocial);
                    //get the valores for this trimester

                    var $chks = $(".chkgroup[data-trimestre='" + trimestreId + "'] input[type='checkbox']");
                    var obj = [];
                    for (var i = 0; i < $chks.length; i++) {
                        obj.push(getCheck($chks[i]));
                    }

                    if (decision != null && decision != undefined) {
                        var data = { obj: JSON.stringify(decision), chks: JSON.stringify(obj) };
                        $.post(url, data, function (result) {
                            if (result != null && result != undefined) {
                                if (result.status) {
                                    if (!result.partidaTermino)
                                    {
                                        //pretty much refresh the page
                                        window.location.href = window.location.href;
                                    }
                                    else
                                    {
                                        //take users to the reports page
                                        window.location.href = "Reportes.aspx?partidaid=" + partidaID;
                                    }
                                }
                                else {
                                    showError("Error al tomar la decisión de esta ronda. Asegure que todos los campos requeridos estén completados.", "error");
                                    return false;
                                }
                            }

                        }, "json");
                    }
                }
                else {
                    showError("Necesitas contestar todas las preguntas de los valores.", "error");
                    return false;
                }
            }
            else {
                showError("Todos los campos son requeridos.", "error")
                return false;
            }
            //end validateDecision
        }
    });

    var checkForUpdates = true;
    //obtains frequent updates
    if ($("#ulJugadores").data("ultimotrimestre") == "False") {
        setInterval(function () {
            var url = servicesUrl + "/Decisiones.ashx?revisarestado=true&partidaid=" + partidaID;

            $.get(url, function (result) {
                if (result != null && result.length > 0) {
                    if (result[0].TrimestreActual == -1) {
                        //if the trimestres are the same, but not the first, this is the end
                        //of the game
                        window.location.href = window.location.href;
                    }

                    //if the current trimestre is greater than the one is loaded in the page,
                    //that means all players have played for this semester
                    if (result[0].TrimestreActual > $("#ulJugadores").data("trimestreactual") + 1) {
                        window.location.href = window.location.href;
                    }

                    $("#ulJugadores li:visible").each(function () {
                        var isIt = true;

                        var player = (getNameById($(this).data("usuarioid"), result) ? $(this).data("nombre") : undefined);

                        if (player == undefined) {
                            showError("El usuario <b>" + $(this).data("nombre") + "</b> ha jugado esta ronda.", "success");
                            $(this).fadeOut();
                        }
                    });

                }
            });
        }, 20000);
    }
});

function getNameById(id, result) {
    for (var i = 0; i < result.length; i++) {
        if (result[i].ID == id) {
            return true;
        }
    }
}

function validateValores(trimestreId) {
    var flag = true;
    $(".chkgroup[data-trimestre='" + trimestreId + "']").each(function () {
        var selected = $(this).find("input:checked").length;
        if (selected != 3) {
            flag = false;
            return false;
        }
        flag = true;
    });
    return flag;
}

function validateDecision(trimestreId) {
    var elems = new Array();
    var flag = true;
    elems.push($("input[name='credito_" + trimestreId + "']"));
    elems.push($("input[name='prestamo_" + trimestreId + "']"));
    elems.push($("input[name='ahorro_" + trimestreId + "']"));
    elems.push($("input[name='certificado_" + trimestreId + "']"));
    elems.push($("input[name='mercadeo_" + trimestreId + "']"));
    elems.push($("input[name='iyd_" + trimestreId + "']"));
    elems.push($("input[name='respsocial_" + trimestreId + "']"));

    var firstError = -1;

    for (var i = 0; i < elems.length; i++) {
        if (elems[i].val() == "") {
            elems[i].parent().parent().parent().addClass("error");
            flag = false;

            firstError = (firstError == -1 ? i : firstError);
        }
    }

    if (!flag)
        elems[firstError].focus();

    return flag;

}
