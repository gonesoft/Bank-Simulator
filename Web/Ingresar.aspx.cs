﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;

public partial class Ingresar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Form.DefaultButton = this.btnLogin.UniqueID;
        this.Form.DefaultFocus = this.txtEmail.ClientID;
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!string.IsNullOrEmpty(txtEmail.Text) && !string.IsNullOrEmpty(txtPassword.Text))
            {
                string email = txtEmail.Text;
                string pass = txtPassword.Text;

                Usuario usr = Usuario.ObtenerUsuarioPorCorreoElectronico(email);

                if (usr != null)
                {
                    if (Cryptology.Verify(pass, usr.ContrasenaEncriptada))
                    {
                        //usuario logueado
                        (Master as Main).UsuarioLogueado = usr;
                        Role role = Role.ObtenerPorID(usr.RoleID);

                        if (role != null)
                        {
                            (Master as Main).Rol = role;
                        }

                        if (Request.QueryString["urlretorno"] != null)
                            Response.Redirect(Request.QueryString["urlretorno"].ToString());
                        else
                            Response.Redirect("~/Default.aspx");

                        return;
                    }
                }

                HTMLHelper.mostrarMensaje(this, "Usuario o Contraseña invalidos.", TipoMensaje.Error);
            }
        }
    }
}