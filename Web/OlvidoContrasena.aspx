﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="OlvidoContrasena.aspx.cs" Inherits="OlvidoContrasena" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="ingresar">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            Recuperar Contraseña
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="span3">&nbsp;</div>
                        <div class="span6">
                            <div class="sign-in-container">
                                <div class="login-wrapper">
                                    <div class="header">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <h3>Recuperar Contraseña<img src="img/logo1.png" alt="Logo" class="pull-right" /></h3>
                                                <p>Inserte su correo electr&oacute;nico</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="widget-body">
                                        <div class="control-group">
                                            <label class="control-label">
                                                Correo Electr&oacute;nico
                                            </label>
                                            <div class="controls controls-row">
                                                <asp:TextBox ID="txtEmail" runat="server" name="txtEmail" CssClass="input span12 email" placeholder="Correo Electr&oacute;nico" Text=""></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="actions">
                                            <asp:Button CssClass="btn btn-danger" Text="Iniciar Proceso" runat="server" ID="btnIniciarProceso" OnClick="btnIniciarProceso_Click" />
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span3">&nbsp;</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#<%= btnIniciarProceso.ClientID %>").on("click", function () {
                $("#ingresar control-group").removeClass("error");
                reset();
                var tmp = $("#<%= txtEmail.ClientID %>");

                if (tmp.val() === "") {
                    mostrarError("Correo Electrónico requerido.", tmp, true);
                    return false;
                }
                else {
                    mostrarError("", tmp, false);
                }
            });
        });
    </script>
</asp:Content>

