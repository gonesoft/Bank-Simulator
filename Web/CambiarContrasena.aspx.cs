﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

public partial class CambiarContrasena : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (txtCurrentPass.Text == "" || txtPassword.Text == "" || txtRepeatPassword.Text == "" || txtRepeatPassword.Text != txtPassword.Text)
            {
                HTMLHelper.mostrarMensaje(this, "Ha ocurrido un error. Revise los campos e intente de nuevo.", TipoMensaje.Error);
                return;
            }

            Usuario usr = (Master as Main).UsuarioLogueado;

            if (usr != null)
            {
                if (Cryptology.Verify(txtCurrentPass.Text, usr.ContrasenaEncriptada))
                {
                    string encrypt = Cryptology.EncryptPassword(txtPassword.Text);
                    usr.Contrasena = encrypt;
                    usr.ContrasenaEncriptada = encrypt;

                    usr = Usuario.Modificar(usr);
                    if (usr != null)
                    {
                        Session["PasswordChanged"] = true;
                        Session.Remove("UsuarioLogueado");
                        Session.Remove("MiBanco");
                        Session.Remove("Rol");
                        Response.Redirect("~/Ingresar.aspx");
                    }
                }
            }
        }
        HTMLHelper.mostrarMensaje(this, "Ha ocurrido un error. Revise los campos e intente de nuevo.", TipoMensaje.Error);
    }
}