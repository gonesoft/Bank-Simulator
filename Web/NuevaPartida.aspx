﻿<%@ Page Title="Crear Nueva Partida" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="NuevaPartida.aspx.cs" Inherits="NuevaPartida" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/select2.css" rel="stylesheet" />
    <script src="js/select2.js"></script>
    <script src="js/select2_locale_es.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        Crear nueva Partida
                      <span class="mini-title">Todos los campos son requeridos
                      </span>
                    </div>
                    <span class="tools">
                        <a class="fs1" aria-hidden="true" data-icon="" data-original-title=""></a>
                    </span>
                </div>
                <div class="widget-body">
                    <div class="control-group">
                        <label class="control-label" for="email1">
                            Nombre de la Partida
                        </label>
                        <div class="controls">
                            <asp:TextBox runat="server" ID="txtNombrePartida" CssClass="span6" placeholder="Nombre de Partida" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password1">
                            Cantidad de Rondas
                        </label>
                        <div class="controls">
                            <asp:DropDownList CssClass="span6" runat="server" ID="ddlTrimestres">
                                <asp:ListItem Value="-1">Seleccione</asp:ListItem>
                                <asp:ListItem Value="2">2 Rondas</asp:ListItem>
                                <asp:ListItem Value="5">5 Rondas</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="repPassword">
                            Jugadores
                        </label>
                        <div class="controls">
                            <asp:HiddenField ID="hiddenUsuarios" runat="server" />
                        </div>
                    </div>
                    <br style="clear: both" />
                    <br style="clear: both" />
                    <br style="clear: both" />
                    <div class="form-actions no-margin">
                        <asp:Button ID="btnCrearPartida" runat="server" CssClass="btn btn-info pull-right" Text="Crear Partida" OnClick="btnCrearPartida_Click" />
                        <div class="clearfix">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
    <script>
        $(function () {
            $('#<%= hiddenUsuarios.ClientID.ToString() %>').addClass("span6");
            $('#<%= hiddenUsuarios.ClientID.ToString() %>').select2({
                minimumInputLength: 1,
                multiple: true,
                placeholder: "Seleccione Jugadores",
                ajax: {
                    url: '<%= ResolveUrl("~/Services/Usuarios.ashx?action=obtenerparcial") %>',
                    dataType: 'json',
                    data: function (term, page) {
                        return {
                            q: term
                        };
                    },
                    results: function (data, page) {
                        return { results: data };
                    }
                }
            });
        });
    </script>
</asp:Content>

