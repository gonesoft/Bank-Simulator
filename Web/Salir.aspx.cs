﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Salir : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Abandon();
        //destroy cookie
        try
        {
            CookiesHelper.DestroyAuthCookie(this);
        }
        catch
        { }

        if (Request.QueryString["urlretorno"] != null)
        {
            if (Session["Rol"] != null)
            {
                BLL.Role r = (BLL.Role)Session["Rol"];
                if (r != null)
                {
                    if (r.Nombre == "Administrator")
                    {
                        Response.Redirect("~/Default.aspx");
                        return;
                    }
                }
            }

            Response.Redirect(Request.QueryString["urlretorno"].ToString());
        }
        else
            Response.Redirect("~/Default.aspx");
    }
}