﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

public partial class CrearCuenta : BaseAdministrationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            List<Role> roles = Role.ObtenerTodos().ToList();
            if (roles != null)
            {
                ddlRole.DataSource = roles;
                ddlRole.DataValueField = "ID";
                ddlRole.DataTextField = "Nombre";
                ddlRole.DataBind();
            }
        }
    }

    protected void Btn_CrearUsuario(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (txtNombre.Text != "" && txtNombreBanco.Text != "" && txtEmail.Text != "" && ddlRole.SelectedItem.Value != "-1")
            {
                if ((Master as Main).EsAdministrador())
                {
                    string rmdContrasena = Cryptology.GenerateRandomCode(10);
                    try
                    {
                        Usuario usr = Usuario.CrearUsuario(txtNombre.Text, rmdContrasena, txtEmail.Text, Convert.ToInt32(ddlRole.SelectedItem.Value), (Master as Main).UsuarioLogueado.ID, true);
                        //creamos el banco
                        if (usr != null)
                        {
                            Banco banco = Banco.CrearBanco(txtNombreBanco.Text, usr.ID, true);
                            if (banco != null)
                            {
                                //creado con satisfaccion
                                MailHelper.sendEmail(usr.Email, "<html><head><title></title></head><body><p>Hola,<strong>" + usr.Nombre + "</strong></p><p><strong>Bienvenido al programa Banquero Joven Popular</strong>. Queremos que aprendas a conocer el lado humano de las finanzas.</p><p><strong>Para acceder al simulador debes ingresar con los siguientes datos:</strong></p><p><strong>Usuario</strong>: " + usr.Email + "<br /><strong>Contraseña</strong>: " + rmdContrasena + " <br /><strong>URL de Acceso</strong>: <a href='http://www.simulator.jadom.org'>www.simulator.jadom.org</a></p><p>Este es el primer paso para convertirte en un profesional ético, respetuoso de los demás y comprometido con tu comunidad, y lograrás el éxito en tus emprendimientos.</p><p>¡Gracias por tu tiempo!</p><p><br />Banquero Joven Popular<br /><a href='http://www.simulator.jadom.org'>www.simulator.jadom.org</a></p></body></html>", "Bienvenido al Simulador Banquero Joven Popular");
                                HTMLHelper.mostrarMensaje(this, "Usuario creado satisfactoriamente.", TipoMensaje.Success);
                                this.txtEmail.Text = "";
                                this.txtNombre.Text = "";
                                this.txtNombreBanco.Text = "";
                                this.ddlRole.SelectedIndex = 0;
                            }
                            else
                            {
                                //error creando banco, eliminar usuario
                                Usuario.Eliminar(usr);
                            }
                        }
                    }
                    catch
                    {
                        HTMLHelper.mostrarMensaje(this, "Este correo electronico ya existe. Por favor utilice otro correo.", TipoMensaje.Error);
                        return;
                    }
                }
                else
                {
                    //no tiene permisos
                }
            }
            else
            {
                //mostrar mensaje, campos invalidos
            }
        }
        else
        {
            return;
        }
    }
}