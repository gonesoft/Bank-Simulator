﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

public partial class Jugar : System.Web.UI.Page
{
    public Partida Partida;

    public int UltimoTrimestreJugado;
    public int TrimestreActual;
    static Random rnd = new Random();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!Page.IsPostBack)
        {
            List<BLL.Producto> products = BLL.Producto.obtenerTodos().GetRandomElements(3); ;
            int r = rnd.Next(products.Count);
            this.repProducts.DataSource = products;
            this.repProducts.DataBind();
        }
        
        if (Request.QueryString["partidaid"] != null)
        {
            if (Session["JugadaExitosa"] != null && (bool)Session["JugadaExitosa"])
            {
                Session.Remove("JugadaExitosa");
                HTMLHelper.mostrarMensaje(this, "La jugada ha sido satisfactoria. Si ha sido el ultimo en jugar este semestre podrá hacer su jugada para el proximo semestre, de lo contrario debe esperar a que todos los demás jugadores jueguen.", TipoMensaje.Success);
            }
            int partidaId;

            if (int.TryParse(Request.QueryString["partidaid"], out partidaId))
            {
                if (!UsuarioPartida.HanTomadoAcciones(partidaId))
                {
                    partidaInvalida("NoComenzada");
                }

                Partida = Partida.ObtenerPorID(partidaId);

                if (Partida != null)
                {
                    //El ultimo trimestre jugado se refiere al ultimo trimestre que el usuario
                    //logueado jugo. Este no siempre va a coincidir con el TrimestreActual, que
                    //representa el trimestre que se deberia jugar proximo.
                    UltimoTrimestreJugado = Jugada.ObtenerUltimoTrimestreJugadoPorBancoIDYPartidaID((Master as Main).MiBanco.ID, Partida.ID);
                    TrimestreActual = Jugada.ObtenerTrimestreActualPorPartidaID(Partida.ID);

                    if (TrimestreActual == Partida.CantidadTrimestres)
                    {
                        Response.Redirect("~/Reportes.aspx?partidaid=" + Partida.ID);
                    }


                    this.TomaDecisiones.Partida = Partida;
                    this.TomaDecisiones.Banco = (Master as Main).MiBanco;
                    this.TomaDecisiones.TrimestreActual = TrimestreActual;
                    this.TomaDecisiones.UltimoTrimestreJugado = UltimoTrimestreJugado;

                    this.BarraJuego.Partida = Partida;
                    this.BarraJuego.TrimestreActual = TrimestreActual;
                    this.BarraJuego.Banco = this.TomaDecisiones.Banco;
                }
                else
                {
                    partidaInvalida();
                }
            }
            else
            {
                partidaInvalida();
            }
        }
        else 
        {
            Response.Redirect("~/Default.aspx");
        }
        
    }

    void partidaInvalida(string nombreSesion = "PartidaInvalida")
    {
        Session[nombreSesion] = true;
        Response.Redirect("~/Default.aspx");
    }
}