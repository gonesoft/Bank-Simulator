﻿using System;
using System.Web.UI;
using BLL;
using System.Collections.Generic;
using System.Linq;

public partial class Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MostrarMensaje("Usted no ha sido invitado a esta partida.", TipoMensaje.Error, "NoInvitado");
        MostrarMensaje("La partida no es valida.", TipoMensaje.Error, "PartidaInvalida");
        MostrarMensaje("Esta partida ya ha iniciado.", TipoMensaje.Error, "HanIniciado");
        MostrarMensaje("Invitaci&oacute;n aceptada satisfactoriamente. Cuando todos los usuarios hayan aceptado o rechazado la invitaci&oacute;n, usted recibir&aacute; un correo electr&oacute;nico con mas instrucciones.", TipoMensaje.Success, "PartidaAceptada");
        MostrarMensaje("Invitaci&oacute;n rechazada satisfactoriamente.", TipoMensaje.Success, "PartidaRechazada");
        MostrarMensaje("Aun faltan usuarios por aceptar esta partida.", TipoMensaje.Error, "NoComenzada");

        if (!Page.IsPostBack && (Master as Main).UsuarioLogueado != null)
        {
            List<Partida> partidas = Partida.ObtenerPorUsuarioID((Master as Main).UsuarioLogueado.ID).ToList();

            if (partidas != null && partidas.Count > 0)
            {
                repPartida.DataSource = partidas;
                repPartida.DataBind();
            }
        }
    }

    void MostrarMensaje(string Mensaje, TipoMensaje tipoMensaje, string NombreSession)
    {
        if (Session[NombreSession] != null)
        {
            HTMLHelper.mostrarMensaje(this, Mensaje, tipoMensaje);
            Session.Remove(NombreSession);
        }
    }

    public string getEstado(object id, object e)
    {
        bool estado = (bool)e;
        if (!estado)
            return "Terminada";

        if (UsuarioPartida.HanTomadoAcciones(Convert.ToInt32(id)))
        {
            return "Comenzada";
        }

        return "No Comenzada";
    }

    public string getUrl(object a, object id)
    {
        bool? aceptada = (bool?)a;

        if (aceptada == null)
        {
            return ResolveUrl("~/AceptarInvitacion.aspx") + "?partidaid=" + Convert.ToInt32(id);
        }

        return ResolveUrl("~/Jugar.aspx") + "?partidaid=" + Convert.ToInt32(id);
    }
}