﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="CambiarContrasena.aspx.cs" Inherits="CambiarContrasena" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel runat="server" ID="panelForm" DefaultButton="btnChangePassword">
        <div id="ingresar">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                                Cambiar Contraseña
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="span3">&nbsp;</div>
                            <div class="span6">
                                <div class="sign-in-container">
                                    <div class="login-wrapper">
                                        <div class="header">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <h3>Cambiar Contraseña<img src="img/logo1.png" alt="Logo" class="pull-right" /></h3>
                                                    <p>Introduzca su antigua contraseña, y la contraseña deseada</p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="widget-body">
                                            <div class="control-group">
                                                <label class="control-label">
                                                    Contraseña Actual
                                                </label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="txtCurrentPass" runat="server" name="txtCurrentPass" CssClass="input span12" TextMode="Password"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">
                                                    Contraseña Nueva
                                                </label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="input span12" TextMode="Password"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">
                                                    Repita Contraseña Nueva
                                                </label>
                                                <div class="controls controls-row">
                                                    <asp:TextBox ID="txtRepeatPassword" runat="server" CssClass="input span12" TextMode="Password"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <asp:Button CssClass="btn btn-danger" Text="Ingresar" runat="server" ID="btnChangePassword" OnClick="btnChangePassword_Click" />
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">&nbsp;</div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#<%= btnChangePassword.ClientID %>").on("click", function () {
                $("#ingresar control-group").removeClass("error");
                reset();
                var tmp = $("#<%= txtCurrentPass.ClientID %>");

                if (tmp.val() === "") {
                    mostrarError("Contraseña Actual requerida.", tmp, true);
                    return false;
                }
                else {
                    mostrarError("", tmp, false);
                }

                tmp = $("#<%= txtPassword.ClientID %>");

                if (tmp.val() === "") {
                    mostrarError("Contraseña Nueva requerida.", tmp, true);
                    return false;
                }
                else {
                    mostrarError("", tmp, false);
                }

                tmp = $("#<%= txtRepeatPassword.ClientID %>");

                if (tmp.val() === "") {
                    mostrarError("Repetir Contraseña Nueva requerida.", tmp, true);
                    return false;
                }
                else {
                    mostrarError("", tmp, false);
                }

                if (tmp.val() != $("#<%= txtPassword.ClientID %>").val()) {
                    mostrarError("Las contraseñas son diferentes.", tmp, true);
                    return false;
                }
                else {
                    mostrarError("", tmp, false);
                }
            });
        });
    </script>
</asp:Content>

