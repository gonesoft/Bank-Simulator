﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Jugar.aspx.cs" Inherits="Jugar" %>

<%@ Register Src="~/WebControls/TomaDecisiones.ascx" TagPrefix="uc1" TagName="TomaDecisiones" %>
<%@ Register Src="~/WebControls/BarraJuego.ascx" TagPrefix="uc1" TagName="BarraJuego" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.bootstrap-money-field.js"></script>
    <script src="js/jquery.bootstrap-percentage-field.js"></script>
    <script src="js/moneyformat.js"></script>
    <script src="js/logic/decisiones.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.sparkline.js"></script>

    <script type="text/javascript">
        var partidaID = new Number('<%= Partida.ID %>');
        var miBancoID = new Number('<%= (Master as Main).MiBanco.ID %>');
        var ultimoTrimestreJugado = new Number('<%= TrimestreActual %>');
    </script>

    <style>
        .correct
        {
            color: green;
        }

        .incorrect {
            color: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-header">
                        <div class="title">
                            <%= this.Partida.Nombre %>
                            <span class="mini-title">Toma de Decisiones
                            </span>
                        </div>
                    </div>
                    <!-- comienzan los tabs -->
                    <uc1:TomaDecisiones runat="server" ID="TomaDecisiones" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
    <script type="text/javascript">
        $(function () {
            $(".tab-pane input[type='text']").on("blur", function () {
                if ($(this).val() != "") {
                    $(this).parent().parent().parent().removeClass("error");
                }
                else if ($(this).val() == "") {
                    $(this).parent().parent().parent().addClass("error");
                }
            });

            //does not allow to click on disabled tabs
            $("#mainTabs > li > a[href='']").on("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
            })

            //checks before submitting that all valores have been answered
            $("#btnJugar").on("click", function () {
                
            });

            $(".chkgroup input[type='checkbox']").on("click", function (event) {
                var howManyChecked = $(this).closest(".chkgroup").find("input:checked").length;
                if (howManyChecked > 3)
                    event.preventDefault();
            });

            //formats money and percentage fields
            $('.money').money_field({ width: "100%" });
            $('.percentage').percentage_field({ width: "100%" });

            getDecisions();

            /* Indicators by trimester */
            $('#capacidad-crediticia').sparkline('html', {
                type: 'bar',
                barColor: '#ed6d49',
                barWidth: 6,
                height: 20,
            });

            $('#creditos-pendientes').sparkline('html', {
                type: 'bar',
                barColor: '#ed6d49',
                barWidth: 6,
                height: 20,
            });

            //not needed, leave in case they change their mind
            //$('#depositos-pendientes').sparkline('html', {
            //    type: 'bar',
            //    barColor: '#ed6d49',
            //    barWidth: 6,
            //    height: 20,
            //});

            $('#reservas-capital').sparkline('html', {
                type: 'bar',
                barColor: '#ed6d49',
                barWidth: 6,
                height: 20,
            });

            $('#prestamos-operaciones').sparkline('html', {
                type: 'bar',
                barColor: '#ed6d49',
                barWidth: 6,
                height: 20,
            });

            $('#capital-trabajo').sparkline('html', {
                type: 'bar',
                barColor: '#ed6d49',
                barWidth: 6,
                height: 20,
            });

            function getDecisions() {
                $.ajax({
                    type: "GET",
                    url: servicesUrl + '/Decisiones.ashx?action=obtenertodos&partidaid=<%= Partida.ID %>',
                    cache: true,
                    dataType: "json",
                    success: function (data) {
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                //sets values for previous trimesters
                                //let's also disable these fields

                                //percentages
                                $("input[name='credito_" + i + "']").val(data[i].TasaCredito.toFixed(2)).prop("disabled", true);
                                $("input[name='prestamo_" + i + "']").val(data[i].TasaPrestamo.toFixed(2)).prop("disabled", true);
                                $("input[name='ahorro_" + i + "']").val(data[i].TasaAhorro.toFixed(2)).prop("disabled", true);
                                $("input[name='certificado_" + i + "']").val(data[i].TasaCertificado.toFixed(2)).prop("disabled", true);

                                //money
                                $("input[name='mercadeo_" + i + "']").val(data[i].Mercadeo.format(2)).prop("disabled", true);
                                $("input[name='iyd_" + i + "']").val(data[i].InvestigacionDesarrollo.format(2)).prop("disabled", true);
                                $("input[name='respsocial_" + i + "']").val(data[i].ResponsabilidadSocial.format(2)).prop("disabled", true);

                                if ((i + 1) <= data.length) {
                                    $("input[name='credito_" + (i + 1) + "']").attr("placeholder", data[i].TasaCredito.toFixed(2));
                                    $("input[name='prestamo_" + (i + 1) + "']").attr("placeholder", data[i].TasaPrestamo.toFixed(2));
                                    $("input[name='ahorro_" + (i + 1) + "']").attr("placeholder", data[i].TasaAhorro.toFixed(2));
                                    $("input[name='certificado_" + (i + 1) + "']").attr("placeholder", data[i].TasaCertificado.toFixed(2));

                                    //dinero
                                    $("input[name='mercadeo_" + (i + 1) + "']").attr("placeholder", (data[i].Mercadeo.format(2)));
                                    $("input[name='iyd_" + (i + 1) + "']").attr("placeholder", data[i].InvestigacionDesarrollo.format(2));
                                    $("input[name='respsocial_" + (i + 1) + "']").attr("placeholder", data[i].ResponsabilidadSocial.format(2));
                                }
                            }
                        }
                    }
                });
            }
        });

        function setTab(trimestre) {
            $('.nav-tabs a[href="#trimestre_' + trimestre + '"]').tab('show');
        }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="rightBar" runat="server">
    <uc1:BarraJuego runat="server" ID="BarraJuego" />
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        Productos Logrados
                      <span class="mini-title">Disponibles en tu banco
                      </span>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="row-fluid">
                        <div class="metro-nav">
                            <asp:Repeater runat="server" ID="repProducts">
                                <ItemTemplate>
                                    <div class="metro-nav-block span3" style="background-color: #00498f">
                                        <a href="#" data-original-title="">
                                            <div class="fs1" aria-hidden="true" data-icon=""></div>
                                            <div class="brand">
                                                <%# Eval("Nombre") %>
                                            </div>
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
