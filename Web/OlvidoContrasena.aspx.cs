﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

public partial class OlvidoContrasena : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnIniciarProceso_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (txtEmail.Text != "" && txtEmail.Text.IndexOf("@") > 0)
            {
                Usuario usr = Usuario.ObtenerUsuarioPorCorreoElectronico(txtEmail.Text);

                if (usr != null)
                {
                    string newPass = BLL.Cryptology.GenerateRandomCode(10);
                    string encrypted = BLL.Cryptology.EncryptPassword(newPass);
                    usr.Contrasena = encrypted;
                    usr.ContrasenaEncriptada = encrypted;

                    usr = Usuario.Modificar(usr);

                    if (usr != null)
                    {
                        this.txtEmail.Text = "";
                        HTMLHelper.mostrarMensaje(this, "Proceso iniciado satisfactoriamente. Por favor revise su correo electrónico, y siga las instrucciones.", TipoMensaje.Success);
                        MailHelper.sendEmail(usr.Email, "<html><body>Estimado <strong>" + usr.Nombre + "</strong><br /><br />Hemos recibido tu solicitud, y hemos generado una nueva contraseña para que puedas acceder al Simulador Bancario. Para ingresar, accede a <a href='http://simulator.jadom.org/Ingresar.aspx'>http://simulator.jadom.org/Ingresar.aspx</a> con tu correo electrónico y tu contraseña temporal.<br /><br /> Tu Contraseña Temporal Es: <strong>" + newPass + "</strong><br />Tan pronto accedas a tu cuenta, no olvides cambiar tu contraseña a algo que puedas recordar facilmente. Para cambiar tu contraseña, ingresa a <a href='http://simulator.jadom.org/CambiarContrasena.aspx'>éste enlace</a>.<br /><br /> Saludos</body></html>", "Simulador Bancario - Solicitud de Contraseña");
                    }
                    else
                    {
                        HTMLHelper.mostrarMensaje(this, "Lo sentimos, ha ocurrido un error al intentar resetear tu contraseña. Favor intenta mas tarde.", TipoMensaje.Error);
                    }
                }
                else
                {
                    HTMLHelper.mostrarMensaje(this, "Lo sentimos, esta cuenta no existe en nuestro sistema.", TipoMensaje.Error);
                }
            }
            else
            {
                HTMLHelper.mostrarMensaje(this, "Correo electrónico inválido.", TipoMensaje.Error);
            }
        }
    }
}