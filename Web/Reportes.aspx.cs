﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

public partial class Reportes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["partidaid"] != null)
        {
            int partidaId;

            if (int.TryParse(Request.QueryString["partidaId"], out partidaId))
            {
                /* Calcula ganador, y los puntos */
                Usuario ganadorMonetario = null;
                Indicador mayorCapital = null;
                Partida partida = Partida.ObtenerPorID(partidaId);

                List<Usuario> usuarios = Usuario.ObtenerUsuariosPorPartidaID(partidaId).ToList();
                List<Indicador> indicadoresFinales = Indicador.ObtenerIndicadoresPorPartidaYTrimestre(partidaId, partida.CantidadTrimestres).ToList();
                List<Banco> bancos = Banco.ObtenerBancosPorPartidaID(partidaId).ToList();
                List<Ganador> ganadores = new List<Ganador>();

                if (usuarios != null && usuarios.Count > 0 && indicadoresFinales != null && indicadoresFinales.Count > 0)
                {
                    //Indicador del ganador

                    mayorCapital = indicadoresFinales.OrderByDescending(indica => indica.obtenerCapital()).FirstOrDefault();

                    //banco ganador
                    Banco banco = (from Banco b in bancos where b.ID == mayorCapital.BancoID select b).FirstOrDefault();

                    //usuario ganador
                    ganadorMonetario = banco.Usuario;

                    foreach (Banco b in bancos)
                    {
                        List<ValorSeleccion> selecciones = ValorSeleccion.ObtenerValoresSeleccionPorPartidaIDYUsuarioID(partidaId, b.Usuario.ID).ToList();
                        int puntosValores = 0;
                        int count = 0;
                        foreach (ValorSeleccion vs in selecciones)
                        {
                            count += (Convert.ToBoolean(vs.Seleccionado) && Convert.ToBoolean(vs.Tipo) ? 1 : 0);
                        }

                        puntosValores = (int)Math.Round(count / 3.75);
                        Ganador jugador = new Ganador();

                            jugador.Nombre = b.Usuario.Nombre;

                        if (b.Usuario.ID == ganadorMonetario.ID)
                        {
                            jugador.PuntosDecisiones = 80;
                            jugador.PuntosValores = puntosValores;
                            jugador.Puntos = jugador.PuntosValores + jugador.PuntosDecisiones;

                            ganadores.Add(jugador);

                        }
                        else
                        {
                            //asignamos puntos a este jugador
                            Indicador uIndicador = (from Indicador i in indicadoresFinales where i.BancoID == b.ID select i).FirstOrDefault();
                            int puntoUsuario = Convert.ToInt32((uIndicador.obtenerCapital() / mayorCapital.obtenerCapital()) * (decimal)0.80 * 100);

                            jugador.PuntosDecisiones = puntoUsuario;
                            jugador.PuntosValores = puntosValores;
                            jugador.Puntos = jugador.PuntosValores + jugador.PuntosDecisiones;
                            ganadores.Add(jugador);

                        }
                    }

                    ganadores = ganadores.OrderByDescending(x => x.Puntos).ToList();
                    ganadores.ForEach(x => x.Lugar = NumberHelper.AOrdinal((ganadores.IndexOf(x) + 1)) + " Lugar");
                    this.repResultado.DataSource = ganadores;
                    this.repResultado.DataBind();
                    
                }

                /*Obtiene las tasas y el resto de la informacion de los reportes */

                List<Decision> decisiones = Decision.ObtenerDecisionesPorPartidaYBanco(partidaId, (Master as Main).MiBanco.ID).ToList();

                if (decisiones != null && decisiones.Count > 0)
                {
                    decisiones.RemoveRange(0, 1);
                    this.repTasas.DataSource = decisiones;
                    this.repTasas.DataBind();

                    this.repInversion.DataSource = decisiones;
                    this.repInversion.DataBind();
                }

                /*Obtiene los indicadores de esta partida y banco */
                List<Indicador> indicadoresBancoPartida = Indicador.ObtenerIndicadoresPorBancoIDYPartidaID((Master as Main).MiBanco.ID, partidaId).ToList();

                if (indicadoresBancoPartida != null && indicadoresBancoPartida.Count > 0)
                {
                    indicadoresBancoPartida.RemoveRange(0, 1);
                    repMovimientos.DataSource = indicadoresBancoPartida;
                    repMovimientos.DataBind();
                }

                /* obtiene los valores seleccionados */
                List<Enunciado> enunciados = Enunciado.ObtenerEnunciadosSeleccionadosPorUsuarioIDYPartidaID(partidaId, (Master as Main).UsuarioLogueado.ID).ToList();
                this.repValores.DataSource = enunciados;
                this.repValores.DataBind();
            }
            else
            {
                //partida id invalida
            }
        }
    }

    public string getCapital(object o)
    {
        return ((Indicador)o).obtenerCapital().ToMoney();
    }
}

public class Ganador
{
    public string Nombre { get; set; }
    public int Puntos { get; set; }
    public string Lugar { get; set; }
    public int PuntosDecisiones { get; set; }
    public int PuntosValores { get; set; }
}